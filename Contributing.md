## Team Members

- Alessandro Speggiorin (2268690s@student.gla.ac.uk)
- Matthew Mcconnell (2235757m@student.gla.ac.uk) 
- Morgan Mcdonald (2326172m@student.gla.ac.uk)
- Pierre Ezard (2250393e@student.gla.ac.uk)
- Ruairidh Macgregor (2250079m@student.gla.ac.uk)

---

## Team Coach (ASEP)

Archit Gupta (2229675g@student.gla.ac.uk)