"""Serialiser Module for all our Models (Part of the Django Rest Framework)"""

from rest_framework import serializers
from cool_glidings.models import Generator, WeatherData, Location
from cool_glidings.DataProcessing.helperFunctions.coords import check_lat, check_lon

# Serializes generator objects into valid data transferrable format


class GeneratorSerializer(serializers.ModelSerializer):
    """Serializer for the Generator"""

    class Meta:
        model = Generator
        fields = ('powerCoefficient', 'radius', 'height', 'label',)

    def validate_powerCoefficient(self, value):
        """Validates the Power Coefficient - Should be > 0

        Arguments:
            value {float} -- power coefficient value to be validated

        Raises:
            serializers.ValidationError

        Returns:
            bool -- True if 0 < x <= 1 otherwise false
        """
        if value < 0 or value > 1:
            raise serializers.ValidationError(
                "Power Coefficient must be between 0 and 1")
        return value

    def validate_radius(self, value):
        """Validates the radius - Should be > 0

        Arguments:
            value {float} -- radius value to be validated

        Raises:
            serializers.ValidationError

        Returns:
            bool -- True if 0 < x otherwise false
        """
        if value <= 0:
            raise serializers.ValidationError(
                "Radius must be a positive number")
        return value

    def validate_height(self, value):
        """Validates the height - Should be > 0

        Arguments:
            value {float} -- height value to be validated

        Raises:
            serializers.ValidationError

        Returns:
            bool -- True if 0 < x otherwise false
        """
        if value <= 0:
            raise serializers.ValidationError(
                "Height must be a positive number")
        return value

# Serializes weather data objects into valid data transferrable format


class WeatherDataSerializer(serializers.ModelSerializer):
    """Serialiser for the Weather Data"""

    class Meta:
        model = WeatherData
        fields = ('date', 'speed', 'pressure', 'humidity', 'temperature',
                  'lon', 'lat', 'time',)

    def validate_pressure(self, value):
        """Validates the pressure - Should be > 0

        Arguments:
            value {float} -- pressure value to be validated

        Raises:
            serializers.ValidationError

        Returns:
            bool -- True if 0 < x otherwise false
        """
        if value < 0:
            raise serializers.ValidationError("Pressure must be non-negative")
        return value

    def validate_temperature(self, value):
        """Validates the temperature - Should be > 0

        Arguments:
            value {float} -- temperature value to be validated

        Raises:
            serializers.ValidationError

        Returns:
            bool -- True if 0 < x otherwise false
        """
        if value < 0:
            raise serializers.ValidationError(
                "Temperature(K) must be non-negative")
        return value

    def validate_speed(self, value):
        """Validates the speed - Should be > 0

        Arguments:
            value {float} -- speed value to be validated

        Raises:
            serializers.ValidationError

        Returns:
            bool -- True if 0 < x otherwise false
        """
        if value < 0:
            raise serializers.ValidationError(
                "Speed(m/s) must be non-negative")
        return value

    def validate_humidity(self, value):
        """Validates the humidity - Should be > 0 and < 100

        Arguments:
            value {float} -- humidity value to be validated

        Raises:
            serializers.ValidationError

        Returns:
            bool -- True if 0 < x <= 100 otherwise false
        """
        if value < 0 or value > 100:
            raise serializers.ValidationError(
                "Humidity(%) must be between 0 and 100")
        return value

    def validate_lat(self, value):
        """Validates the latitude

        Arguments:
            value {float} -- latitude value to be validated

        Raises:
            serializers.ValidationError

        Returns:
            bool -- True if -90 <= x <= 90 otherwise false
        """
        if not check_lat(value):
            raise serializers.ValidationError(
                "Latitude must be between -90 and 90 degrees")
        return value

    def validate_lon(self, value):
        """Validates the longitude

        Arguments:
            value {float} -- longitude value to be validated

        Raises:
            serializers.ValidationError

        Returns:
            bool -- True if -180 <= x <= 180 otherwise false
        """
        if not check_lon(value):
            raise serializers.ValidationError(
                "Longitude must be between -180 and 180 degrees")
        return value


# Serializes location objects into valid data transferrable format
class LocationSerializer(serializers.ModelSerializer):
    """Serialiser for the location model"""

    class Meta:
        model = Location
        fields = ('name', 'lat', 'lon',)

    def validate_lat(self, value):
        """Validates the latitude

        Arguments:
            value {float} -- latitude value to be validated

        Raises:
            serializers.ValidationError

        Returns:
            bool -- True if -90 <= x <= 90 otherwise false
        """
        if not check_lat(value):
            raise serializers.ValidationError(
                "Latitude must be between -90 and 90 degrees")
        return value

    def validate_lon(self, value):
        """Validates the longitude

        Arguments:
            value {float} -- longitude value to be validated

        Raises:
            serializers.ValidationError

        Returns:
            bool -- True if -180 <= x <= 180 otherwise false
        """
        if not check_lon(value):
            raise serializers.ValidationError(
                "Longitude must be between -180 and 180 degrees")
        return value
