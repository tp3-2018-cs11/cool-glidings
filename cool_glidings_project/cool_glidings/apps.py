"""
Includes any application configuration for the app
"""
from django.apps import AppConfig


class CoolGlidingsConfig(AppConfig):
    """
    Class which represents configuration items for
    the app cool_glidings
    """
    name = 'cool_glidings'
