""" Testing weather retrieval of current data """

import pytest
from cool_glidings.DataProcessing.helperFunctions.unassorted import (
    data_fetch,
    haveInternetConnectionToURL
)
from cool_glidings.DataProcessing.currentWeather.currentWeatherRetrieval import (
    data_organizer,
    get_weather_data_now
)


FULL_URL = ("http://api.openweathermap.org/data/2.5/weather?"
            "lat=55&lon=-4&mode=json&APPID=9bea93a9b3ab6e2327925caf7f789e47")
RAW_DICT = data_fetch(FULL_URL)
LAT = 55.0
LON = -4.0

# Boolean variable to check wether ther is internet connection or not.
INTERNET_CONNECTION = haveInternetConnectionToURL(FULL_URL)


@pytest.mark.skipif(not INTERNET_CONNECTION, reason="There is no internet connection")
def test_data_organizer():
    """Has the data filtered correctly?"""
    assert 'pressure' in data_organizer(RAW_DICT, LAT, LON)
    assert 'humidity' in data_organizer(RAW_DICT, LAT, LON)
    assert 'wind_speed' in data_organizer(RAW_DICT, LAT, LON)
    assert 'time' in data_organizer(RAW_DICT, LAT, LON)
    assert 'time' in data_organizer(RAW_DICT, LAT, LON)
    assert 'temp' in data_organizer(RAW_DICT, LAT, LON)
    assert 'lat' in data_organizer(RAW_DICT, LAT, LON)
    assert 'lon' in data_organizer(RAW_DICT, LAT, LON)


@pytest.mark.skipif(not INTERNET_CONNECTION, reason="There is no internet connection")
def test_get_weather_data_forecast():
    """ Is the return type a list of dictionaries """
    assert isinstance(get_weather_data_now(LAT, LON), dict)


@pytest.mark.skipif(not INTERNET_CONNECTION, reason="There is no internet connection")
def test_get_weather_data_now():
    """ Is the return type a list of dictionaries """
    assert isinstance(get_weather_data_now(LAT, LON), dict)
    # Check that when given wrong LAT LON it returns None
    assert get_weather_data_now(-91, 0) is None
