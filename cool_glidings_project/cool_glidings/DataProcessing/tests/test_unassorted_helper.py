""" Testing the unassorted helper functions """

import re
import pytest
from cool_glidings.DataProcessing.helperFunctions.unassorted import (
    data_fetch,
    data_post,
    get_url,
    generateUniqueLabel,
    haveInternetConnectionToURL
)


FULL_URL = ("http://api.openweathermap.org/data/2.5/weather?"
            "lat=55.0&lon=-4.0&mode=json&APPID=9bea93a9b3ab6e2327925caf7f789e47")
RAW_DICT = data_fetch(FULL_URL)
LAT = 55.0
LON = -4.0


# Boolean variable to check wether ther is internet connection or not.
INTERNET_CONNECTION = haveInternetConnectionToURL(FULL_URL)


def test_generate_unique_label():
    """ Test if the label generated matches the default UUID4 pattern """

    # Default pattern for UUID4 generated IDs
    regex_true = r'^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$'

    # Wrong pattern for UUID4 generated IDs
    regex_false = r'^[0-9A-F]{3}-[0-9A-F]{4}-[4][0-9A-F]{8}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$'

    assert isinstance(generateUniqueLabel(), str)
    assert re.match(regex_true, generateUniqueLabel(),
                    re.IGNORECASE) is not None
    assert re.match(regex_false, generateUniqueLabel(), re.IGNORECASE) is None


# Tests that it doesn't have internet connection (to simulate this I use a invalid url)
def test_have_internet_connection_to_url_false():
    """ Check if there is not internet connection """
    assert not haveInternetConnectionToURL("")


def test_get_url():
    """Does the url compile as expected"""
    assert get_url(LAT, LON, "weather") == FULL_URL
    assert get_url(LAT, LON, "forecast") == ("http://api.openweathermap.org/data/2.5/forecast?"
                                             "lat=55.0&lon=-4.0&mode=json&"
                                             "APPID=9bea93a9b3ab6e2327925caf7f789e47")


@pytest.mark.skipif(not INTERNET_CONNECTION, reason="There is no internet connection")
def test_data_fetch():
    """Does the dictionary of raw data contain the correct pairs?"""
    data = data_fetch(FULL_URL)
    assert data is not None
    assert "coord" in data
    assert "main" in data
    assert "wind" in data

@pytest.mark.skipif(INTERNET_CONNECTION, reason="There is internet connection")
def test_data_fetch_no_internet():
    """Does function handles exceptions correctly?"""
    assert data_fetch(FULL_URL) is None


# Use of "https://httpstat.us/" which is a service that simply returns whatever code
# you append on the end of it e.g. "https://httpstat.us/201" returns a 201 response
@pytest.mark.skipif(not INTERNET_CONNECTION, reason="There is no internet connection")
def test_data_fetch_negative_response():
    """ Check if the function returns a None if the URL is invalid """
    assert data_fetch("https://httpstat.us/400") is None


@pytest.mark.skipif(not INTERNET_CONNECTION, reason="There is no internet connection")
def test_data_fetch_bad_url():
    """ Check that the function returns none if the URL is invalid or not in the right format """
    assert data_fetch("http://thiswillfail.com") is None

# Use of "https://httpstat.us/" which is a service that simply returns whatever code
# you append on the end of it e.g. "https://httpstat.us/201" returns a 201 response


def test_data_post_valid_url_and_response():
    """ Should return true on 201 """
    assert data_post({}, "https://httpstat.us/201") is True


def test_data_post_valid_url_but_bad_response():
    """ Should return false on 400 """
    assert data_post({}, "https://httpstat.us/400") is False


def test_data_post_invalid_url():
    """ Shoudl return false on bad url """
    assert data_post({}, "x") is False
