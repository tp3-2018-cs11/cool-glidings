""" Testing datetime helper functions """

from cool_glidings.DataProcessing.helperFunctions.datetimeStuff import (
    time_converter,
    date_converter,
    changeDateFormat,
    isDateFormatValid,
    isTimeFormatValid
)


def test_time_converter():
    """Does the timestamp generate as expected?"""
    assert time_converter(1542045600) == "1800"


def test_date_converter():
    """ Does the datestamp generate as expected? """
    assert date_converter(1542045600) == "12-11-2018"


# Test if changeDateFormat returns a valid formatted String date
def test_change_date_format():
    """ Checking if the change of date formats works """

    # Check that the returned value is a str
    assert isinstance(changeDateFormat("12-11-2003"), str)
    # Check that the returned values is as expected
    assert changeDateFormat("12-11-2003") == "2003-11-12"

    # Check for invalid date formats
    assert changeDateFormat("12/11/2003") is False
    assert changeDateFormat("33-11-2004") is False
    assert changeDateFormat("test") is False
    assert changeDateFormat(10) is False

# Test if isDateFormatValid returns validates Dates correctly


def test_is_date_format_valid():
    """ Testing if the date format checks work correctly """

    # Check that the returned values is as expected
    assert isDateFormatValid("2004-11-12") is True
    assert isDateFormatValid("2004-1-2") is True

    # Check for invalid date formats
    assert isDateFormatValid("12/11/2003") is False
    assert isDateFormatValid("33-11-2004") is False
    assert isDateFormatValid("test") is False
    assert isDateFormatValid("") is False
    assert isDateFormatValid(5) is False


def test_is_time_format_valid():
    """ Testing if the time format checks work correctly """

    # Check that the time is in the valid format
    assert isTimeFormatValid("12:20") is True
    assert isTimeFormatValid("00:00") is True

    # Check for invalid time formats
    assert isTimeFormatValid("23:60") is False
    assert isTimeFormatValid("23-60") is False
    assert isTimeFormatValid("test") is False
    assert isTimeFormatValid("") is False
    assert isTimeFormatValid(5) is False
