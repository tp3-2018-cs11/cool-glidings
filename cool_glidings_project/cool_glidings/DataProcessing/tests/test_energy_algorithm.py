""" Tests for the Energy Algorithm file functions """

from cool_glidings.DataProcessing.energyOutput.EnergyAlgorithm import (
    DRY_AIR_MOLAR_MASS,
    WATER_VAPOUR_MOLAR_MASS,
    UNIVERSAL_GAS_CONSTANT,
    swept_area,
    kelvin_to_celsius,
    water_vapour_pressure,
    saturation_vapour_pressure,
    dry_air_pressure,
    combined_air_pressure,
    energyOutput)


def test_dry_air_molar_mass_correct():
    """ Checking the constant is correct """
    assert DRY_AIR_MOLAR_MASS == 0.028964


def test_water_vapour_molar_mass_correct():
    """ Checking the constant is correct """
    assert WATER_VAPOUR_MOLAR_MASS == 0.018016


def test_universal_gas_constant_correct():
    """ Checking the constant is correct """
    assert UNIVERSAL_GAS_CONSTANT == 8.314


def test_swept_area_calculated_correctly():
    """ Should allow only positive input """
    assert round(swept_area(10), 3) == 314.159
    assert swept_area(0) == 0
    assert round(swept_area(2), 3) == 12.566
    assert swept_area(-1) is None


def test_kelvin_to_celsius_converter_calculates_correctly():
    """ Should only allow non-negative input """
    assert kelvin_to_celsius(-1) is None
    assert kelvin_to_celsius(0) == -273.15
    assert kelvin_to_celsius(1) == -272.15
    assert kelvin_to_celsius(273.15) == 0
    assert round(kelvin_to_celsius(456.98), 2) == 183.83


def test_water_vapour_pressure_calculated_correctly():
    """ Should only allow non-negative input """
    assert water_vapour_pressure(-1, 10) is None
    assert water_vapour_pressure(1, -1) is None
    assert water_vapour_pressure(-1, -1) is None
    assert water_vapour_pressure(12, 541) == 64.92


def test_saturation_vapour_pressure_calculated_correctly():
    """ Should only accept valid celcius temperatures """
    assert round(saturation_vapour_pressure(0), 4) == 6.1078
    assert round(saturation_vapour_pressure(-1), 4) == 5.6773
    assert round(saturation_vapour_pressure(1), 5) == 6.56686
    assert saturation_vapour_pressure(-274) is None
    assert round(saturation_vapour_pressure(274), 4) == 63833.938


def test_dry_air_pressure_calculated_correctly():
    """ Should only accept non-negative input """
    assert dry_air_pressure(-1, -1) is None
    assert dry_air_pressure(-1, 10) is None
    assert dry_air_pressure(10, -1) is None
    assert round(dry_air_pressure(10.12, 5.04), 2) == 5.08
    assert dry_air_pressure(5, 10) is None
    assert dry_air_pressure(5, None) is None


def test_combined_air_pressure_calculated_correctly():
    """ Should only accept non-negative input """
    assert combined_air_pressure(-1, 10, 10) is None
    assert combined_air_pressure(1, None, 10) is None
    assert combined_air_pressure(1, 10, -10) is None
    assert round(combined_air_pressure(13, 7, 321), 7) == 0.0001637


def test_energy_output_calculated_correctly():
    """ Checks if it get the right answer """
    assert energyOutput(-1, 0.3, 50, 285, 100, 10) is None
    assert energyOutput(10, 0.3, 50, 285, -1, 10) is None
    assert energyOutput(10, 0.3, 50, 285, 100, -1) is None
    assert energyOutput(10, 1.3, 50, 285, 100, 10) is None
    assert energyOutput(10, -0.3, 50, 285, 100, 10) is None
    assert round(energyOutput(10, 0.3, 50, 285, 100, 10), 4) == 56.0912
