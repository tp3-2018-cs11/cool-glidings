""" Testing the weather retrieval of the forecast """

import pytest
from cool_glidings.DataProcessing.DataRetrieval.retrieval import (
    data_fetch,
    data_organizer,
    get_weather_data_forecast
)
from cool_glidings.DataProcessing.helperFunctions.unassorted import haveInternetConnectionToURL


FULL_URL = ("http://api.openweathermap.org/data/2.5/forecast?"
            "lat=55&lon=-4&mode=json&APPID=9bea93a9b3ab6e2327925caf7f789e47")
RAW_DICT = data_fetch(FULL_URL)
LAT = 55.0
LON = -4.0

# Boolean variable to check wether ther is internet connection or not.
INTERNET_CONNECTION = haveInternetConnectionToURL(FULL_URL)


@pytest.mark.skipif(not INTERNET_CONNECTION, reason="There is no internet connection")
def test_data_organizer():
    """Has the data filtered correctly?"""
    assert 'pressure' in data_organizer(RAW_DICT, LAT, LON)[0]
    assert 'humidity' in data_organizer(RAW_DICT, LAT, LON)[0]
    assert 'wind_speed' in data_organizer(RAW_DICT, LAT, LON)[0]
    assert 'time' in data_organizer(RAW_DICT, LAT, LON)[0]
    assert 'time' in data_organizer(RAW_DICT, LAT, LON)[0]
    assert 'temp' in data_organizer(RAW_DICT, LAT, LON)[0]
    assert 'lat' in data_organizer(RAW_DICT, LAT, LON)[0]
    assert 'lon' in data_organizer(RAW_DICT, LAT, LON)[0]


@pytest.mark.skipif(not INTERNET_CONNECTION, reason="There is no internet connection")
def test_get_weather_data_forecast():
    """ Is the return type a list of dictionaries """
    assert isinstance(get_weather_data_forecast(LAT, LON), list)
    assert isinstance(get_weather_data_forecast(LAT, LON)[0], dict)
    assert get_weather_data_forecast(-91, 0) is None
