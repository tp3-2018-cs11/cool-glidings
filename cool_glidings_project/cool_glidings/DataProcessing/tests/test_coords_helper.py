""" Testing the coordinate helper functions """

from cool_glidings.DataProcessing.helperFunctions.coords import (
    check_coords_format,
    check_lat,
    check_lon
)


def test_check_coords_single_functions():
    """ Testing the checking of latitude and longitude """

    # Check out of bounds values
    assert check_lat(91.0) is False
    assert check_lat(-91.0) is False
    assert check_lon(181.0) is False
    assert check_lon(-181.0) is False

    # Test integers
    assert check_lat(1) is False
    assert check_lon(1) is False

    # Test string
    assert check_lat("Test") is False
    assert check_lon("Test") is False

    # Test boundaries
    assert check_lat(90.0) is True
    assert check_lat(-90.0) is True
    assert check_lon(180.0) is True
    assert check_lon(-180.0) is True

    # Test in-range value
    assert check_lat(45.0) is True
    assert check_lon(45.0) is True


def test_check_coords_format():
    """ Testing the checking of coordinates """

    # test out of bounds floats
    assert check_coords_format(200.0, 50.0) is False
    assert check_coords_format(200.0, 50.0) is False
    # test integers
    assert check_coords_format(0, 1) is False
    # test string
    assert check_coords_format("Hello World", "Blah") is False
    # test boundaries
    assert check_coords_format(90.0, 180.0) is True
    assert check_coords_format(-90.0, -180.0) is True
    # test in-range value
    assert check_coords_format(55.0, -4.0) is True
