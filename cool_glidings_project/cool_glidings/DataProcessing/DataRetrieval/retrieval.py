""" Based on this Python script found on StackExchange:
https://codereview.stackexchange.com/questions/131371/script-to-print-weather-report-from-openweathermap-api
Altered to fit our own needs
"""

from cool_glidings.DataProcessing.helperFunctions.unassorted import data_fetch, get_url
from cool_glidings.DataProcessing.helperFunctions.datetimeStuff import (
    time_converter,
    date_converter
)
from cool_glidings.DataProcessing.helperFunctions.coords import check_coords_format


def data_organizer(raw_dict, lat, lon):
    """Create a list of dictionaries of relevant data of each day
       from the dictionary made of the entire response from data_fetch function

    Arguments:
        raw_dict {dict} -- raw json dictionary of weather points from openweathermap api
        lat {float} -- latitude
        lon {float} -- longitude

    Returns:
        list(dict) -- all the weather points retrieved formatted
    """
    dict_list = []
    for index in range(len(raw_dict['list'])):
        dict_list.append(dict(
            lat=lat,
            lon=lon,
            temp=raw_dict['list'][index]['main']['temp'],
            pressure=raw_dict['list'][index]['main']['pressure'],
            humidity=raw_dict['list'][index]['main']['humidity'],
            wind_speed=raw_dict['list'][index]['wind']['speed'],
            time=time_converter(raw_dict['list'][index]['dt']),
            date=date_converter(raw_dict['list'][index]['dt'])
        ))
    return dict_list


def get_weather_data_forecast(lat, lon):
    """ The function that uses all functions above to get a list of dictionaries of
    weather data points for the given latitude and longitude
    Will return none if there was an error and will print out the error into the terminal """

    if not check_coords_format(lat, lon):
        print("The coordinates were not in the right format.")
        print("They must be floats with a -90 <= lat <= 90 and -180 <= lon <= 180")
        print("The lat and lon given were " + str(lat) + " and " + str(lon))
        return None

    api_data = data_fetch(get_url(lat, lon, "forecast"))

    if not api_data:
        print("Something went wrong with the api call...")
        return None

    return data_organizer(api_data, lat, lon)
