""" Contains the algorithm to produce an energy output of a wind turbine from parameters given """

from math import pi

# Constants
WATER_VAPOUR_MOLAR_MASS = 0.018016
DRY_AIR_MOLAR_MASS = 0.028964
UNIVERSAL_GAS_CONSTANT = 8.314


def energyOutput(radius, power_coefficient, humidity, kelvin_temp, absolute_pressure, speed):
    """ Gives a single integer figure of the expected energy output of a wind turbine given:
        - radius (length of wind turbine blade)
        - power_coefficient (or wind turbine)
        - humidity (%)
        - temparature (in Kelvin)
        - pressure (absolute in hPa)
        - speed (meters per second)

    Returns energy output in Watts (returns None if one of the parameters were invalid)
    """

    area = swept_area(radius)

    # Calculating pressure
    p_sat = saturation_vapour_pressure(kelvin_to_celsius(kelvin_temp))
    p_water = water_vapour_pressure(humidity, p_sat)
    p_dry = dry_air_pressure(absolute_pressure, p_water)
    air_density = combined_air_pressure(p_water, p_dry, kelvin_temp)

    # Return None if any of the values are None (i.e. one of the parameters were invalid)
    if area is None or air_density is None:
        return None

    if speed < 0 or power_coefficient > 1 or power_coefficient < 0:
        return None

    return 0.5 * air_density * area * (speed**3) * power_coefficient


def swept_area(radius):
    """ Calculates the Swept Area Given a Radius (in Meters) of the Wind Turbine Blades """

    if radius < 0:
        return None

    return pi * radius**2


def kelvin_to_celsius(kelvin_temp):
    """ Converts a Kelvin Temperature to Celsius """

    if kelvin_temp < 0:
        return None

    return kelvin_temp - 273.15


def water_vapour_pressure(humidity, saturation_pressure):
    """ Calculates the Water Vapour Pressure given a humidity and Saturation Pressure """

    if saturation_pressure is None or humidity < 0 or saturation_pressure < 0:
        return None

    return (humidity / 100.0) * saturation_pressure


def saturation_vapour_pressure(celsius_temp):
    """ Calculates the Saturation Vapour Pressure given a Temperature in Celsisus """

    if celsius_temp is None or celsius_temp < -273.15:
        return None

    power = (7.5 * celsius_temp) / (celsius_temp + 237.3)

    return 6.1078 * 10**power


def dry_air_pressure(absolute_pressure, w_vapour_pressure):
    """Calculates the Dry Air Pressure given the Absolute Pressure and the Water Vapour Pressure"""

    if absolute_pressure is None or w_vapour_pressure is None:
        return None

    if absolute_pressure < 0 or w_vapour_pressure < 0:
        return None

    if absolute_pressure < w_vapour_pressure:
        return None

    return absolute_pressure - w_vapour_pressure


def combined_air_pressure(w_vapour_pressure, d_air_pressure, kelvin_temp):
    """Calculates the Specific Pressure of Air given the
    Water Vapour and Dry Pressure and Temperature """

    if not w_vapour_pressure or not d_air_pressure or not kelvin_temp:
        return None

    if w_vapour_pressure < 0 or d_air_pressure < 0 or kelvin_temp < 0:
        return None

    vapour_mass = d_air_pressure * DRY_AIR_MOLAR_MASS + w_vapour_pressure * WATER_VAPOUR_MOLAR_MASS

    return vapour_mass / (UNIVERSAL_GAS_CONSTANT * kelvin_temp)
