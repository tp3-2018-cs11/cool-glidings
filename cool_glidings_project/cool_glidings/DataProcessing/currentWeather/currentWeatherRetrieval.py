"""
Altered code from our retrieval script for future weather data

Based on this Python script found on StackExchange:
https://codereview.stackexchange.com/questions/131371/script-to-print-weather-report-from-openweathermap-api
Altered to fit our own needs
"""

from cool_glidings.DataProcessing.helperFunctions.unassorted import (
    data_fetch,
    get_url
)
from cool_glidings.DataProcessing.helperFunctions.datetimeStuff import (
    date_converter,
    time_converter
)
from cool_glidings.DataProcessing.helperFunctions.coords import check_coords_format


def data_organizer(raw_dict, lat, lon):
    """Create a list of dictionaries of relevant data of each day from
    the dictionary made of the entire response from data_fetch function

    Arguments:
        raw_dict {dict} -- raw json dictionary of current weather point
        lat {float} -- latitude
        lon {float} -- longitude

    Returns:
        dict -- dictionary of the weather point formatted
    """

    data_dict = dict(
        lat=lat,
        lon=lon,
        temp=raw_dict['main']['temp'],
        pressure=raw_dict['main']['pressure'],
        humidity=raw_dict['main']['humidity'],
        wind_speed=raw_dict['wind']['speed'],
        time=time_converter(raw_dict['dt']),
        date=date_converter(raw_dict['dt'])
    )
    return data_dict


def get_weather_data_now(lat, lon):
    """Returns a dictionary of a weather point for the given coordinates

    Arguments:
        lat {float} -- latitude
        lon {float} -- longitude

    Returns:
        dict -- dictionary of the weather point or None if something went wrong
    """

    if not check_coords_format(lat, lon):
        print("The coordinates were not in the right format.)")
        print("They must be floats with a -90 <= lat <= 90 and -180 <= lon <= 180")
        print("The lat and lon given were " + str(lat) + " and " + str(lon))
        return None

    api_data = data_fetch(get_url(lat, lon, "weather"))

    if not api_data:
        print("Something went wrong with the api call...")
        return None

    print("Run Successfully")
    return data_organizer(api_data, lat, lon)
