""" Functions that check that your coordinates are valid """

# Function to check if the coordinates entered are valid
def check_coords_format(lat, lon):
    """ Checks if latitude and longitude are in the right format """
    return check_lat(lat) and check_lon(lon)

# Function to check if latitude is valid
def check_lat(lat):
    """ Checks if latitude is valid (-90 <= x <= 90) """
    if isinstance(lat, float):
        return -90 <= lat <= 90
    return False

# Function to check if longitude is valid
def check_lon(lon):
    """ Checks if longitude is valid (-180 <= x <= 180) """
    if isinstance(lon, float):
        return -180 <= lon <= 180
    return False
