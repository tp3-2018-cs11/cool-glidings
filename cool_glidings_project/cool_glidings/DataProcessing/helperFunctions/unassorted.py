""" General unassorted functions """
import uuid
import urllib
from urllib import request, parse
import json

API_KEY = '9bea93a9b3ab6e2327925caf7f789e47'

def generateUniqueLabel():
    """ Generates a unique UUID4 code and returns it as a String """
    return str(uuid.uuid4())


def haveInternetConnectionToURL(url):
    """ Takes a url string and checks if there is an internet connection to it """
    try:
        urllib.request.urlopen(url)
        return True
    except:
        return False


# Create the full API making use of latitude and longitude and generated API key
# Type is a string giving the type of weather retrieval to use
def get_url(lat, lon, type):
    """ Generates API url dinamically """
    api = "http://api.openweathermap.org/data/2.5/" + type + "?lat="
    full_url = api + str(lat) + '&lon=' + str(lon) + \
        '&mode=json&APPID=' + API_KEY
    return full_url


# Make the API call and load the results from the JSON response into a dictionary
def data_fetch(full_url):
    """ Makes the API call and returns a JSON response """
    try:
        with urllib.request.urlopen(full_url) as url:
            output = url.read().decode('utf-8')
            raw_dict = json.loads(output)
            return raw_dict
    except:
        return None

# Function used to perform a POST request.
# The function takes 2 parameters:
# - data: a dictionary of data that has to be sent as part of the POST request
# - full_url: website url
# The function returns True for success, False for failure


def data_post(data, full_url):
    """  Executes a post request to the specified url """
    try:
        # Encode the data dictionary
        post_data = parse.urlencode(data).encode()
        # Make the post request
        req = request.Request(full_url, data=post_data)
        resp = request.urlopen(req)
        return resp.getcode() == 201
    except:
        return False
