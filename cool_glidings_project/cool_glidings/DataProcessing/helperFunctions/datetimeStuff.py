""" Functions to check and convert dates and datetimes in various forms """

import datetime
import re

def time_converter(time):
    """ Format timestamp to return time in 24 hour format """
    converted_time = datetime.datetime.fromtimestamp(
        int(time)
    ).strftime('%H%M')
    return converted_time

def date_converter(date):
    """ Format timestamp to return date in a day-month-year format """
    converted_date = datetime.datetime.fromtimestamp(
        int(date)
    ).strftime('%d-%m-%Y')
    return converted_date


# Gets a string input in the format dd-mm-yyyy and returs a string of the format yyyy-mm-dd
# NOTE:
# - It is important that the string passed is an actual STRING and not a date object
def changeDateFormat(date_string):
    """ Changes the data format of a date string from dd-mm-yyyy to yyyy-mm-dd """
    if isinstance(date_string, str):
        # Validate the date using regual expressions. The yyyy value can only be between 1000-2999
        if bool(re.match(r'^(0[1-9]|[12][0-9]|3[01])\-(0[1-9]|1[0-2])\-([12][0-9]{3})$', date_string)):
            return datetime.datetime.strptime(date_string, '%d-%m-%Y').strftime('%Y-%m-%d')
    # If something fails, return False
    return False

# The function checks if a date is in the format yyyy-mm-dd
# NOTE:
# - It is important that the string passed is an actual STRING and not a date object
def isDateFormatValid(date_string):
    """ Checks if a date string is of the format yyyy-mm-dd """
    if isinstance(date_string, str):
        # Check if the date is in the format yyyy-mm-dd
        return bool(re.match(r'^([12][0-9]{3})\-(0?[1-9]|1[0-2])\-(0?[1-9]|[12][0-9]|3[01])$', date_string))
    # If something fails, return False
    return False


def isTimeFormatValid(time_string):
    """ Checks if a time string has the format HH:MM """
    if isinstance(time_string, str):
        return bool(re.match(r'^([01]\d|2[0-3]):?([0-5]\d)$', time_string))
    return False
