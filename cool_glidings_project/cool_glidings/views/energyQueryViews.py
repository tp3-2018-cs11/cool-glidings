""" REST end point for energy queries """

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework_csv import renderers as r
from rest_framework.settings import api_settings
from django.db.models import Q
from cool_glidings.models import (
    Generator,
    WeatherData,
    Location
)
from cool_glidings.DataProcessing.helperFunctions.datetimeStuff import (
    isDateFormatValid,
    isTimeFormatValid
)
from cool_glidings.DataProcessing.energyOutput.EnergyAlgorithm import energyOutput


class EnergyQuery(APIView):

    """ Energy query django rest framework api view """

    renderer_classes = tuple(
        api_settings.DEFAULT_RENDERER_CLASSES) + (r.CSVRenderer, )

    @staticmethod
    def validate_location(loc):
        """Checks if the location name is valid

        Arguments:
            loc {string} -- location name to check

        Returns:
            bool -- True if it exists, otherwise false
        """
        if loc == '':
            return (False, "Please specify a location.")

        if not Location.objects.filter(name=loc).exists():
            return (False, "Location name was not recognised.")

        return (True, loc)

    @staticmethod
    def validate_generator(gen):
        """Checks that the generator is valid

        Arguments:
            gen {str} -- generator name

        Returns:
            bool -- True if it exists in the database, otherise false
        """
        if gen == '':
            return (False, "Please specify a generator.")

        if not Generator.objects.filter(label=gen).exists():
            return (False, "Generator name was not recognised.")

        return (True, gen)

    @staticmethod
    def params_check(params):
        """checks that the dictionary of parameters in the get request are valid

        Arguments:
            params {dict} -- dictionary of parameter

        Returns:
            (bool, dict) -- True and cleaned/formatted data if it is all valid,
                            false and empty otherwise
        """

        clean_data = params

        errors = {}
        no_errors = True

        # Check if  location is not empty and valid
        eval_loc = EnergyQuery.validate_location(clean_data.get('loc', ''))
        if eval_loc[0]:
            clean_data['loc'] = eval_loc[1]
        else:
            errors['loc'] = eval_loc[1]
        no_errors &= eval_loc[0]

        # Check if the generator label is not empty and valid
        eval_gen = EnergyQuery.validate_generator(clean_data.get('gen', ''))
        if eval_gen[0]:
            clean_data['gen'] = eval_gen[1]
        else:
            errors['gen'] = eval_gen[1]
        no_errors &= eval_gen[0]

        # Checking if the dates are valid - if they are empty
        # then we will retrieve all possible weather points
        if clean_data.get('startDate', '') != '' and not isDateFormatValid(clean_data.get('startDate', '')):
            errors['startDate'] = "Start Date should be in the yyyy-mm-dd format."
            no_errors &= False

        if clean_data.get('endDate', '') != '' and not isDateFormatValid(clean_data.get('endDate', '')):
            errors['endDate'] = "End Date should be in the yyyy-mm-dd format."
            no_errors &= False

        # Checking if the times are valid - if they are empty then we will retrieve all possible weather points
        if clean_data.get('startTime', '') != '' and not isTimeFormatValid(clean_data.get('startTime', '')):
            errors['start_time'] = "Start Time should be in the HH:MM 24 hour format."
            no_errors &= False

        if clean_data.get('endTime', '') != '' and not isTimeFormatValid(clean_data.get('endTime', '')):
            errors['endTime'] = "End Time should be in the HH:MM 24 hour format."
            no_errors &= False

        return (True, clean_data) if no_errors else (False, errors)

    @staticmethod
    def weather_data_query(start_date, end_date, start_time, end_time, lat, lon):
        """
        - Returns all weather data objects that meet the SQL contraints
        - start_date - if an empty string is provided then the earliest date is used
        - end_date - if an empty string is provided then the latest date is used
        - start_time - if an empty string is provided then there are no constraints
        - end_time - if an empty string is provided then there are no constraints
        - lat - latitude coordiantes of the generator
        - lon - longitude coordinates of the generator
        """

        # Make sure lat and lon are not None as these are required
        if lat is None or lon is None:
            return None

        # Get relevant wetaher data
        if not start_date:
            if not end_date:
                # Since there is no start or end date, then there are no times to deal with and only latitude and longitude needs to be taken into account
                w_data = WeatherData.objects.filter(Q(lat=lat) & Q(lon=lon))
            else:
                w_data = WeatherData.objects.filter(Q(lat=lat) & Q(lon=lon) & Q(date__lte=end_date))
        elif not end_date:
            if not start_time:
                w_data = WeatherData.objects.filter(Q(lat=lat) & Q(lon=lon) & Q(date__gte=start_date))
            else:
                w_data = WeatherData.objects.filter(Q(lat=lat) & Q(lon=lon) & ((Q(time__gte=start_time) & Q(date=start_date)) | Q(date__gt=start_date)))
        else:
            if not start_time:
                if not end_time:
                    w_data = WeatherData.objects.filter(Q(lat=lat) & Q(lon=lon) & Q(date__gte=start_date) & Q(date__lte=end_date))
                else:
                    w_data = WeatherData.objects.filter(Q(lat=lat) & Q(lon=lon) & Q(date__gte=start_date) & (Q(date__lt=end_date) | (Q(date=end_date) & Q(time__lte=end_time))))
            else:
                if not end_time:
                    w_data = WeatherData.objects.filter(Q(lat=lat) & Q(lon=lon) & Q(date__lte=end_date) & (Q(date__gt=start_date) | (Q(date=start_date) & Q(time__gte=start_time))))
                else:
                    w_data = WeatherData.objects.filter(Q(lat=lat) & Q(lon=lon) & (Q(date__lt=end_date) | (Q(date=end_date) & Q(time__lte=end_time))) & (Q(date__gt=start_date) | (Q(date=start_date) & Q(time__gte=start_time))))

        # Make sure dates entered were valid
        if not w_data:
            print("Error: no weather data found from given dates")
            return None

        return w_data

    @staticmethod
    def compute_energy(weather_data, generator):
        """
        The function take weather_data (QuerySet) and a generator as parameters.
        It computes the energy estimate for every weather_data value specific to
        that generator.
        The function returns a dictionary where each key (energyList),(dates)
        and (times) is mapped to a list.
        """

        energy_results = []

        # Compute the energy output for every weather data value.
        for w_data in weather_data:
            energy_results.append({'energy': energyOutput(generator.radius, generator.powerCoefficient, w_data.humidity, w_data.temperature, w_data.pressure * 100.0, w_data.speed),
                                   'date':   str(w_data.date),
                                   'time':   str(w_data.time)})

        return energy_results

    def get(self, request):
        """
        GET request method that should validate parameters given and process the request,
        returning JSON with energy data and time pairs for valid input, or errors respectively.
        """

        # Validating the data and returning 400 - Bad Request if it is invalid
        validated_data = EnergyQuery.params_check(request.query_params.dict())
        if not validated_data[0]:
            return Response(validated_data[1], status=status.HTTP_400_BAD_REQUEST)

        # Keeping only the valid results dictionary
        validated_data = validated_data[1]

        generator = Generator.objects.get(label=validated_data.get('gen'))
        location = Location.objects.get(name=validated_data.get('loc'))

        weather_query_results = EnergyQuery.weather_data_query(validated_data.get("startDate", ""),
                                                               validated_data.get(
                                                                   "endDate", ""),
                                                               validated_data.get(
                                                                   "startTime", ""),
                                                               validated_data.get(
                                                                   "endTime", ""),
                                                               location.lat,
                                                               location.lon)

        # If the weather query results were empty then we couldn't satisfy the request of the dates and times given with not enough weather data being likely
        if not weather_query_results:
            return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        return Response(EnergyQuery.compute_energy(weather_query_results, generator), status=status.HTTP_200_OK)
