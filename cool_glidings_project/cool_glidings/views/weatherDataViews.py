"""API Enpoint for the Weather Data Model"""

from rest_framework import generics
from cool_glidings.models import WeatherData
from cool_glidings.serializers import WeatherDataSerializer



class WeatherDataList(generics.ListCreateAPIView):
    """List API Endpoint"""
    queryset = WeatherData.objects.all()
    serializer_class = WeatherDataSerializer


class WeatherDataDetail(generics.RetrieveUpdateDestroyAPIView):
    """Individual Detail API Endpoint View"""
    queryset = WeatherData.objects.all()
    serializer_class = WeatherDataSerializer
