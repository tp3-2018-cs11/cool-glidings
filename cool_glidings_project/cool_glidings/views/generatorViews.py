"""API End points to deal with the generator models"""

from rest_framework import generics
from cool_glidings.models import Generator
from cool_glidings.serializers import GeneratorSerializer


class GeneratorList(generics.ListCreateAPIView):
    """The API view for the list of generators"""
    queryset = Generator.objects.all()
    serializer_class = GeneratorSerializer


class GeneratorDetail(generics.RetrieveUpdateDestroyAPIView):
    """The API view for a single Generator"""
    queryset = Generator.objects.all()
    serializer_class = GeneratorSerializer
