"""API Endpoints for the location model"""

from rest_framework import generics
from cool_glidings.models import Location
from cool_glidings.serializers import LocationSerializer



class LocationList(generics.ListCreateAPIView):
    """List API Endpoint"""
    queryset = Location.objects.all()
    serializer_class = LocationSerializer


class LocationDetail(generics.RetrieveUpdateDestroyAPIView):
    """Individual Detail API Endpoint View"""
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
