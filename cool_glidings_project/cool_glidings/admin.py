""" Registering models to be used in Admin Page """

from django.contrib import admin
from cool_glidings.models import Generator, WeatherData, Location

admin.site.register(Generator)
admin.site.register(WeatherData)
admin.site.register(Location)
