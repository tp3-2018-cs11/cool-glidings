""" All the url paths we use """

from django.urls import path
from cool_glidings import views

urlpatterns = [
    path('generators/', views.generatorViews.GeneratorList.as_view()),
    path('generators/<str:pk>/', views.generatorViews.GeneratorDetail.as_view()),
    path('weatherData/', views.weatherDataViews.WeatherDataList.as_view()),
    path('weatherData/<int:pk>/', views.weatherDataViews.WeatherDataDetail.as_view()),
    path('locations/', views.locationViews.LocationList.as_view()),
    path('locations/<str:pk>/', views.locationViews.LocationDetail.as_view()),
    path('energyQuery/', views.energyQueryViews.EnergyQuery.as_view())
]
