"""
Tests for the Custom Command created for the retrieval of weather data
"""
from io import StringIO
from django.test import TestCase
from cool_glidings.management.commands.weatherCall import Command


class CustomCommandsTest(TestCase):
    """
    Class which includes the tests for the weather data retrieval command
    """

    def setUp(self):
        """
        Setting up the command class with our own stdout that we can look at
        """
        self.out = StringIO()
        self.command = Command(stdout=self.out)

    def tearDown(self):
        """
        Making sure to close the IO stream afterwards
        """
        self.out.close()

    @staticmethod
    def location_fetch_success(*args):
        """
        To simulate a positive response from the locations api
        """
        return [{"name": "TestLocation", "lat": 20.0, "lon": 20.0}]

    @staticmethod
    def weather_post_success(*args):
        """
        To simulate a positive post to the weather data api
        """
        return True

    @staticmethod
    def weather_post_unsuccessful(*args):
        """
        To simulate a unsuccessful response from the locations api
        """
        return False

    @staticmethod
    def location_fetch_empty(*args):
        """
        To simulate a successful but empty response from the locations api
        """
        return []

    @staticmethod
    def location_fetch_unsuccessful(*args):
        """
        To simulate a unsuccessful response from the locations api
        """
        return None

    def test_successful_api_calls(self):
        """
        Tests the successfull calls to the api
        """
        self.command.get_and_save_current_weather_for_locations(
            CustomCommandsTest.location_fetch_success,
            CustomCommandsTest.weather_post_success)
        self.assertIn("WeatherData object created successfully.",
                      self.out.getvalue())

    def test_empty_location_api_call(self):
        """
        Tests if the locations are empty, the result of the API call
        """
        self.command.get_and_save_current_weather_for_locations(
            CustomCommandsTest.location_fetch_empty,
            CustomCommandsTest.weather_post_success)
        self.assertIn(
            "ERROR: Something failed when retrieving data from the location API",
            self.out.getvalue())

    def test_unsuccessful_location_api_call(self):
        """
        Tests if there is an invalid location, and the response
        from the api
        """
        self.command.get_and_save_current_weather_for_locations(
            CustomCommandsTest.location_fetch_unsuccessful,
            CustomCommandsTest.weather_post_success)
        self.assertIn(
            "ERROR: Something failed when retrieving data from the location API",
            self.out.getvalue())

    def test_unsuccessful_weather_data_post(self):
        """
        Tests if a post request for weather data is unsuccessfull, if there is
        invalid input
        """
        self.command.get_and_save_current_weather_for_locations(
            CustomCommandsTest.location_fetch_success,
            CustomCommandsTest.weather_post_unsuccessful)
        self.assertIn(
            "ERROR: Something failed when creating the WeatherData object.", self.out.getvalue())

    def test_location_api_url_correct(self):
        """
        Tests to make sure the api url is correct for locations
        """
        self.assertEqual(self.command.LOCATIONS_LIST_GET_URL,
                         ("http://django-env.izjypmbxc6.eu-west-2.elasticbeanstalk.com/"
                         "cool_glidings/locations/"))

    def test_weather_data_api_url_correct(self):
        """
        Tests to make sure the api url is correct for the weather data
        """

        self.assertEqual(self.command.WEATHER_DATA_POST_URL,
                         ("http://django-env.izjypmbxc6.eu-west-2.elasticbeanstalk.com/"
                         "cool_glidings/weatherData/"))
