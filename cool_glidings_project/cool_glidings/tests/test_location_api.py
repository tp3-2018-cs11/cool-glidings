"""
Tests regarding the location end point in the rest api
"""
from rest_framework.test import APIClient, APITestCase
from cool_glidings.models import Location
from populate_cool_glidings import populate


class LocationAPITest(APITestCase):
    """
    Class containing tests regarding locations
    """

    def setUp(self):
        """
        Set up the environment before the test begins, by running the
        population script and creating a dumby client
        """
        try:
            populate()
        except ImportError:
            print("The module population_script does not exist")
        except NameError:
            print("The module populate() does not exist or is not correct")
        except:
            print("Something went wrong in the populate() function")
        self.client = APIClient()

    def test_get_locations(self):
        """
        Tests to make sure the server returns a 200 if the
        get request was successfull at retrieving all the
        locations in the database
        """
        request = self.client.get(
            'http://127.0.0.1:8000/cool_glidings/locations/')
        data = request.data[0]
        self.assertEqual(request.status_code, 200)
        self.assertEqual(data['name'], "Carcant Wind Farm")
        self.assertEqual(data['lat'], 55.7660)
        self.assertEqual(data['lon'], 3.0189)

    def test_get_location_specific(self):
        """
        Tests to make sure the server returns a 200 if the
        get request was successfull for a specific locaiton
        """
        request = self.client.get(
            'http://127.0.0.1:8000/cool_glidings/locations/Carcant Wind Farm/')
        data = request.data
        self.assertEqual(request.status_code, 200)
        self.assertEqual(data['name'], "Carcant Wind Farm")
        self.assertEqual(data['lat'], 55.7660)
        self.assertEqual(data['lon'], 3.0189)

    def test_post_location_valid(self):
        """
        Tests to make sure the post request was successfull indicated by a 201
        status code being returns by the server
        """
        request = self.client.post('http://127.0.0.1:8000/cool_glidings/locations/', {
            "name": "Test",
            "lat": 55.7660,
            "lon": 3.0189
        })
        self.assertEqual(request.status_code, 201,
                         "Did not return 201 on valid location POST")

        self.assertTrue(Location.objects.filter(name="Test").exists(
        ), "New location object was not added to the dictionary despite valid post")

    def test_post_location_invalid(self):
        """
        Tests to make sure the server returns a 400 if there is invalid parameters
        passed through when making the post request
        """
        request = self.client.post('http://127.0.0.1:8000/cool_glidings/locations/', {
            "name": "Test",
            "lat": -100,
            "lon": 3.0189
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 on invalid location POST with a latitude below -90")
        self.assertFalse(Location.objects.filter(name="Test").exists(
        ), "New location object was added despite being invalid")

        request = self.client.post('http://127.0.0.1:8000/cool_glidings/locations/', {
            "name": "Test",
            "lat": 100,
            "lon": 3.0189
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 on invalid location POST with a latitude above 90")
        self.assertFalse(Location.objects.filter(name="Test").exists(
        ), "New location object was added despite being invalid")

        request = self.client.post('http://127.0.0.1:8000/cool_glidings/locations/', {
            "name": "Test",
            "lat": -1.0,
            "lon": -181
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 on invalid location POST with a longitude below -180")
        self.assertFalse(Location.objects.filter(name="Test").exists(
        ), "New location object was added despite being invalid")

        request = self.client.post('http://127.0.0.1:8000/cool_glidings/locations/', {
            "name": "Test",
            "lat": -1.0,
            "lon": 181
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 on invalid location POST with a latitude above 180")
        self.assertFalse(Location.objects.filter(name="Test").exists(
        ), "New location object was added despite being invalid")

    def test_put_location_invalid(self):
        """
        Tests to make sure the server returns a 400 status code if the parameters are invalid
        when making a put request
        """
        request = self.client.put("http://127.0.0.1:8000/cool_glidings/"
                                  "locations/Carcant Wind Farm/", {
                                      "name": "Test",
                                      "lat": -100,
                                      "lon": 3.0189
                                  })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 on invalid location PUT with a latitude below -90")

        request = self.client.put("http://127.0.0.1:8000/cool_glidings/"
                                  "locations/Carcant Wind Farm/", {
                                      "name": "Test",
                                      "lat": 100,
                                      "lon": 3.0189
                                  })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 on invalid location PUT with a latitude above 90")

        request = self.client.put("http://127.0.0.1:8000/cool_glidings/"
                                  "locations/Carcant Wind Farm/", {
                                      "name": "Test",
                                      "lat": -1.0,
                                      "lon": -181
                                  })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 on invalid location PUT with a longitude below -180")

        request = self.client.put("http://127.0.0.1:8000/cool_glidings/"
                                  "locations/Carcant Wind Farm/", {
                                      "name": "Test",
                                      "lat": -1.0,
                                      "lon": 181
                                  })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 on invalid location PUT with a latitude above 180")

    def test_put_location_valid(self):
        """
        Tests to make sure the server returns a 200 if the put request was successfull
        """
        request = self.client.put("http://127.0.0.1:8000/cool_glidings/"
                                  "locations/Carcant Wind Farm/", {
                                      'name': 'Carcant Wind Farm',
                                      'lat': 5.0,
                                      'lon': 5.0,
                                  })
        self.assertEqual(request.status_code, 200)

    def test_delete_location(self):
        """
        Tests to make sure the server returns a 204 if the delete request was successfull
        """
        request = self.client.delete(
            'http://127.0.0.1:8000/cool_glidings/locations/Carcant Wind Farm/')
        self.assertEqual(request.status_code, 204,
                         "Did not return 204 response code")
        self.assertFalse(Location.objects.filter(
            name="Carcant Wind Farm").exists(), "Location was not deleted")
