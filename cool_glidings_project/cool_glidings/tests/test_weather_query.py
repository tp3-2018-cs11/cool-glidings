"""
Tests regarding the weather data part of the energy query
"""
from django.test import TestCase
from cool_glidings.models import WeatherData
from cool_glidings.views import EnergyQuery


class WeatherQueryTests(TestCase):
    """
    Class which contains tests testing the weather data part
    of the energy query
    """

    def setUp(self):
        """
        Set up the environment for the tests, by creating test weather data
        """
        WeatherData.objects.create(date="2018-01-01", speed=10.0, pressure=100.0,
                                   humidity=98.0, temperature=300.0, lon=0.0, lat=0.0, time="10:00")
        WeatherData.objects.create(date="2018-01-02", speed=10.0, pressure=100.0,
                                   humidity=98.0, temperature=300.0, lon=0.0, lat=0.0, time="10:00")
        WeatherData.objects.create(date="2018-01-03", speed=10.0, pressure=100.0,
                                   humidity=98.0, temperature=300.0, lon=0.0, lat=0.0, time="10:00")
        WeatherData.objects.create(date="2018-01-04", speed=10.0, pressure=100.0,
                                   humidity=98.0, temperature=300.0, lon=0.0, lat=0.0, time="10:00")
        self.weather_data = list(WeatherData.objects.all())

    def tearDown(self):
        """
        Deletes all test weather data objects created
        """
        WeatherData.objects.all().delete()

    def test_weather_data_query_with_no_coords(self):
        """
        Tests invalid coordinates
        """
        self.assertEqual(EnergyQuery.weather_data_query(
            "", "", "", "", None, 0.2), None, "Did not return None on no latitude given")
        self.assertEqual(EnergyQuery.weather_data_query(
            "", "", "", "", 0.2, None), None, "Did not return None on no longitude given")

    def test_weather_data_query_with_no_datetime(self):
        """
        Tests invalid datetime value
        """
        self.assertEqual(list(EnergyQuery.weather_data_query(
            "", "", "", "", 0.0, 0.0)), self.weather_data,
                         "Did not return all weather data objects")

    def test_weather_data_query_with_start_date(self):
        """
        Tests weather data query when a start date is specified
        """
        self.assertEqual(list(EnergyQuery.weather_data_query("2018-01-02", "", "", "", 0.0, 0.0)),
                         self.weather_data[1:], "Did not exclude weather data before startDate")
        self.assertEqual(list(EnergyQuery.weather_data_query("2018-01-01", "", "", "", 0.0, 0.0)),
                         self.weather_data, "Excluded weather data equal/after the startDate")

    def test_weather_data_query_with_end_date(self):
        """
        Tests weather data query when a end date is specifie
        """
        self.assertEqual(list(EnergyQuery.weather_data_query("", "2018-01-03", "", "", 0.0, 0.0)),
                         self.weather_data[:-1], "Did not exclude weather data after endDate")
        self.assertEqual(list(EnergyQuery.weather_data_query("", "2018-01-04", "", "", 0.0, 0.0)),
                         self.weather_data, "Excluded weather data equal/before the endDate")

    def test_weather_data_query_with_start_date_time(self):
        """
        Tests the weather data query when there is a start data and start time
        """
        self.assertEqual(list(EnergyQuery.weather_data_query(
            "2018-01-01", "", "10:01", "", 0.0, 0.0)),
                         self.weather_data[1:], "Did not exclude weather data before startDateTime")
        self.assertEqual(list(EnergyQuery.weather_data_query(
            "2018-01-01", "", "10:00", "", 0.0, 0.0)),
                         self.weather_data, "Excluded weather data equal/after the startDateTime")

    def test_weather_data_query_with_end_date_time(self):
        """
        Tests the weather data query when there is a end date and end time
        """
        self.assertEqual(list(EnergyQuery.weather_data_query(
            "", "2018-01-03", "", "09:59", 0.0, 0.0)),
                         self.weather_data[:-1], "Did not exclude weather data after endDateTime")
        self.assertEqual(list(EnergyQuery.weather_data_query(
            "", "2018-01-04", "", "10:00", 0.0, 0.0)),
                         self.weather_data, "Excluded weather data equal/before the endDateTime")

    def test_weather_data_query_with_start_date_time_and_end_date(self):
        """
        Tests the weather data query when there is a start date, time and end date
        """
        self.assertEqual(list(EnergyQuery.weather_data_query(
            "2018-01-01", "2018-01-03", "10:01", "", 0.0, 0.0)), self.weather_data[1:3],
                         "Did not exclude weather data that was out of range")

    def test_weather_data_query_with_start_date_and_end_date_time(self):
        """
        Tests the weather data query when there is a start data, end date and time
        """
        self.assertEqual(list(EnergyQuery.weather_data_query(
            "2018-01-02", "2018-01-04", "", "09:59", 0.0, 0.0)), self.weather_data[1:3],
                         "Did not exclude weather data that was out of range")

    def test_weather_data_query_with_start_date_time_and_end_date_time(self):
        """
        Tests the weather data query when there is a start data and time and an end date and time
        """
        self.assertEqual(list(EnergyQuery.weather_data_query(
            "2018-01-01", "2018-01-04", "10:01", "09:59", 0.0, 0.0)),
                         self.weather_data[1:3],
                         "Did not exclude weather data that was out of range")
