"""
Test file for the energy query rest end point
"""

from rest_framework.test import APIClient, APITestCase
from cool_glidings.models import Location, Generator, WeatherData
from cool_glidings.views import EnergyQuery


class EnergyQueryAPITests(APITestCase):
    """
    Class which contains tests for the energy query to the rest API
    """

    def test_validate_location_valid(self):
        """
        Tests the validator for locations
        """
        # Create a new location object
        Location.objects.create(name="Test", lat=55.7660, lon=3.0189)

        # Empty location
        self.assertEqual(EnergyQuery.validate_location(
            "")[0], False, "Did not return False for empty Location String")
        # Location does not exist
        self.assertEqual(EnergyQuery.validate_location("LocationDoesNotExist")[0], False,
                         "Did not return False for Location that does not "
                         "exist in the Database")
        # Valid location
        self.assertEqual(EnergyQuery.validate_location("Test")[0], True,
                         "Did not return True for a valid Location Name that"
                         "exists in the Database")

    def test_validate_generator(self):
        """
        Tests the validator for generators
        """
        # Create a new generator object
        Generator.objects.create(
            label="Test", powerCoefficient=0.5, radius=15, height=20)

        # Empty Generator
        self.assertEqual(EnergyQuery.validate_generator(
            "")[0], False, "Did not return False for empty Generator String")
        # Generator does not exist
        self.assertEqual(EnergyQuery.validate_generator("GeneratorDoesNotExist")[0], False,
                         "Did not return False for Generator that does not exist in the Database")
        # Valid generator
        self.assertEqual(EnergyQuery.validate_generator("Test")[0], True,
                         "Did not return True for a valid Location Name that"
                         "exists in the Database")

    def test_params_checks(self):
        """
        Test regarding parameters to the object trying to be created
        """
        # Create a new location object
        Location.objects.create(name="Location", lat=55.7660, lon=3.0189)
        # Create a new generator object
        Generator.objects.create(
            label="Generator", powerCoefficient=0.5, radius=15, height=20)

        # Invalid location
        self.assertEqual(EnergyQuery.params_check({
            'loc': 'InvalidLocation',
            'gen': 'Generator',
            'startDate': '2018-12-11',
            'endDate': '2019-12-10',
            'startTime': '12:20',
            'endTime': '23:10'
        })[0], False, "Did not return false on invalid location")

        # Invalid Generator
        self.assertEqual(EnergyQuery.params_check({
            'loc': 'Location',
            'gen': 'InvalidGenerator',
            'startDate': '2018-12-11',
            'endDate': '2019-12-10',
            'startTime': '12:20',
            'endTime': '23:10'
        })[0], False, "Did not return false on invalid generator")

        # Invalid StartDate
        self.assertEqual(EnergyQuery.params_check({
            'loc': 'Location',
            'gen': 'Generator',
            'startDate': '2018-20-12',
            'endDate': '2019-12-10',
            'startTime': '12:20',
            'endTime': '23:10'
        })[0], False, "Did not return false on invalid start date")

        # Invalid EndDate
        self.assertEqual(EnergyQuery.params_check({
            'loc': 'Location',
            'gen': 'Generator',
            'startDate': '2018-12-12',
            'endDate': '2019-20-10',
            'startTime': '12:20',
            'endTime': '23:10'
        })[0], False, "Did not return False on invalid end date")

        # Invalid startTime
        self.assertEqual(EnergyQuery.params_check({
            'loc': 'Location',
            'gen': 'Generator',
            'startDate': '2018-12-12',
            'endDate': '2019-12-10',
            'startTime': '30:20',
            'endTime': '23:10'
        })[0], False, "Did not return false on invalid start time")

        # Invalid endTime
        self.assertEqual(EnergyQuery.params_check({
            'loc': 'Location',
            'gen': 'Generator',
            'startDate': '2018-12-12',
            'endDate': '2019-12-10',
            'startTime': '10:20',
            'endTime': '24:10'
        })[0], False, "Did not return false on invalid end time")

        # Valid if startDate,endDate,startTime,endTime are empty
        self.assertEqual(EnergyQuery.params_check({
            'loc': 'Location',
            'gen': 'Generator',
            'startDate': '',
            'endDate': '',
            'startTime': '',
            'endTime': ''
        })[0], True, "Did not return true on valid empty date and time values")

        # Valid input
        self.assertEqual(EnergyQuery.params_check({
            'loc': 'Location',
            'gen': 'Generator',
            'startDate': '2018-12-12',
            'endDate': '2019-12-10',
            'startTime': '10:20',
            'endTime': '22:10'
        })[0], True, "Did not return true on full valid input")

    def test_compute_energy(self):
        """
        Tests the energy value computed from the rest api
        """
        # Create a new generator object
        gen = Generator.objects.create(
            label="Generator", powerCoefficient=0.5, radius=15, height=20)
        # Create a new weatherData object
        weather_data = WeatherData.objects.create(
            date="2019-01-26",
            speed=10.3,
            pressure=990.0,
            humidity=93.0,
            temperature=280.15,
            lon=3.0189,
            lat=55.766,
            time="16:34:00")

        # Valid if the response length is 3
        self.assertEqual(
            len(EnergyQuery.compute_energy([weather_data], gen)[0]), 3,
            "Did not return the correct number of key in the dictionary")

        # Valid if the energy estimate is computed correctly.
        self.assertEqual(
            EnergyQuery.compute_energy([weather_data], gen)[
                0]['energy'], 237717.80474368512,
            "Did not return the correct energy output estimate"
        )
        # Valid if the date returned is as expected
        self.assertEqual(
            EnergyQuery.compute_energy([weather_data], gen)[
                0]['date'], "2019-01-26",
            "Did not return the correct date"
        )
        # Valid if the time returned is as expected
        self.assertEqual(
            EnergyQuery.compute_energy([weather_data], gen)[
                0]['time'], "16:34:00",
            "Did not return the correct time"
        )

    def test_get_invalid_returns_400(self):
        """
        Tests to make sure the server returns a 400 if there is invalid
        parameters
        """
        request = APIClient().get('http://127.0.0.1:8000/cool_glidings/energyQuery/')
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 on invalid params given")

    def test_get_insufficient_weather_data_returns_422(self):
        """
        Tests to make sure the server returs a 422 if the there is no
        data available to compute the energy query
        """
        Location.objects.create(name="Test", lat=20.1, lon=-100.3)
        Generator.objects.create(label="Test")
        request = APIClient().get(
            'http://127.0.0.1:8000/cool_glidings/energyQuery/?loc=Test&gen=Test')
        self.assertEqual(request.status_code, 422,
                         "Did not return 422 with insufficient weather data"
                         "to carry out the request")

    def test_get_valid_returns_200(self):
        """
        Tests to make sure the server returns a 200 if all was successfull
        """
        Location.objects.create(name="Test", lat=20.1, lon=-100.3)
        Generator.objects.create(
            label="Test", radius=10.0, height=20.0, powerCoefficient=0.4)
        WeatherData.objects.create(
            lat=20.1, lon=-100.3, speed=10.0, humidity=98, pressure=100, temperature=300)
        request = APIClient().get(
            'http://127.0.0.1:8000/cool_glidings/energyQuery/?loc=Test&gen=Test')
        self.assertEqual(request.status_code, 200,
                         "Did not return 200 on valid request with sufficient weather data")
