"""
Tests regarding the weather data end point of the api
"""
from rest_framework.test import APIClient, APITestCase
from cool_glidings.models import WeatherData
from populate_cool_glidings import populate


class WeatherDataAPITests(APITestCase):
    """
    Class which contains tests regarding the weather data end point
    """
    def setUp(self):
        """
        Set up the environment for the tests, by running the population
        script and creating a dumby client
        """
        try:
            populate()
        except ImportError:
            print("The module population_script does not exist")
        except NameError:
            print("The module populate() does not exist or is not correct")
        except:
            print("Something went wrong in the populate() function")
        self.client = APIClient()

    def test_get_weather_data(self):
        """
        Tests to make sure the server returns a 200 if the get request for
        all the weather data was successfull
        """
        request = self.client.get(
            'http://127.0.0.1:8000/cool_glidings/weatherData/')
        data = request.data[0]
        self.assertEqual(request.status_code, 200)
        self.assertEqual(data["date"], "2014-08-18")
        self.assertEqual(data["speed"], 15)
        self.assertEqual(data["pressure"], 10.0)
        self.assertEqual(data["lon"], 0.0)
        self.assertEqual(data["lat"], 0.0)
        self.assertEqual(data["time"], "09:34:32")

    def test_get_weather_data_specific(self):
        """
        Tests to make sure the server returns a 200 if there was a successfull
        get request to get a specific weather data object
        """
        request = self.client.get(
            'http://127.0.0.1:8000/cool_glidings/weatherData/1/')
        data = request.data
        self.assertEqual(request.status_code, 200)
        self.assertEqual(data["date"], "2014-08-18")
        self.assertEqual(data["speed"], 15)
        self.assertEqual(data["pressure"], 10.0)
        self.assertEqual(data["lon"], 0.0)
        self.assertEqual(data["lat"], 0.0)
        self.assertEqual(data["time"], "09:34:32")

    def test_post_weather_data_valid(self):
        """
        Tests to make sure the server returns a 201 if a post request was successfull
        """
        request = self.client.post('http://127.0.0.1:8000/cool_glidings/weatherData/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": 15.0,
            "temperature": 24.0,
            "lon": 0.0,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 201,
                         "Did not return 201 after valid POST")
        self.assertEqual(len(WeatherData.objects.all()), 2,
                         "New weather data was not added despite valid POST")

    def test_post_weather_data_invalid(self):
        """
        Tests to make sure the server returns a 400 if there was invalid paramaters
        passed through when making the post request
        """
        request = self.client.post('http://127.0.0.1:8000/cool_glidings/weatherData/', {
            "date": "2018-08-01",
            "speed": -1,
            "pressure": 10.0,
            "humidity": 15.0,
            "temperature": 24.0,
            "lon": 0.0,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid POST with negative speed")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid POST with negative speed")

        request = self.client.post('http://127.0.0.1:8000/cool_glidings/weatherData/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": -10.0,
            "humidity": 15.0,
            "temperature": 24.0,
            "lon": 0.0,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid POST with negative pressure")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid POST with negative pressure")

        request = self.client.post('http://127.0.0.1:8000/cool_glidings/weatherData/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": -15.0,
            "temperature": 24.0,
            "lon": 0.0,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid POST with humidity less than 0")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid POST with humidity less than 0")

        request = self.client.post('http://127.0.0.1:8000/cool_glidings/weatherData/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": 150.0,
            "temperature": 24.0,
            "lon": 0.0,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid POST with humidity more than 100")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid POST with humidity more than 100")

        request = self.client.post('http://127.0.0.1:8000/cool_glidings/weatherData/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": 15.0,
            "temperature": -24.0,
            "lon": 0.0,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid POST with negative kelvin temperature")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid POST with negative kelvin temperature")

        request = self.client.post('http://127.0.0.1:8000/cool_glidings/weatherData/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": 15.0,
            "temperature": 24.0,
            "lon": -181,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid POST with longitude less than -180")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid POST with longitude less than -180")

        request = self.client.post('http://127.0.0.1:8000/cool_glidings/weatherData/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": 15.0,
            "temperature": 24.0,
            "lon": 181,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid POST with longitude more than 180")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid POST with longitude more than 180")

        request = self.client.post('http://127.0.0.1:8000/cool_glidings/weatherData/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": 15.0,
            "temperature": 24.0,
            "lon": 0.0,
            "lat": -91,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid POST with latitude less than -90")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid POST with latitude less than -90")

        request = self.client.post('http://127.0.0.1:8000/cool_glidings/weatherData/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": 15.0,
            "temperature": 24.0,
            "lon": 0.0,
            "lat": 91,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid POST with latitude more than 90")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid POST with latitude more than 90")

    def test_put_weather_data_valid(self):
        """
        Tests ot make sure the server returns a 200 if the put request was successfull
        """
        request = self.client.put('http://127.0.0.1:8000/cool_glidings/weatherData/1/', {
            "date": "2014-08-12",
            "speed": 16,
            "pressure": 10.0,
            "humidity": 15.0,
            "temperature": 24.0,
            "lon": 0.0,
            "lat": 0.0,
            "time": "09:34:32"
        })
        self.assertEqual(request.status_code, 200)

    def test_put_weather_data_invalid(self):
        """
        Tests to make sure the server returns a 400 if the parameters passed in the put request
        were invalid
        """
        request = self.client.put('http://127.0.0.1:8000/cool_glidings/weatherData/1/', {
            "date": "2018-08-01",
            "speed": -1,
            "pressure": 10.0,
            "humidity": 15.0,
            "temperature": 24.0,
            "lon": 0.0,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid put with negative speed")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid put with negative speed")

        request = self.client.put('http://127.0.0.1:8000/cool_glidings/weatherData/1/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": -10.0,
            "humidity": 15.0,
            "temperature": 24.0,
            "lon": 0.0,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid put with negative pressure")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid put with negative pressure")

        request = self.client.put('http://127.0.0.1:8000/cool_glidings/weatherData/1/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": -15.0,
            "temperature": 24.0,
            "lon": 0.0,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid put with humidity less than 0")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid put with humidity less than 0")

        request = self.client.put('http://127.0.0.1:8000/cool_glidings/weatherData/1/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": 150.0,
            "temperature": 24.0,
            "lon": 0.0,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid put with humidity more than 100")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid put with humidity more than 100")

        request = self.client.put('http://127.0.0.1:8000/cool_glidings/weatherData/1/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": 15.0,
            "temperature": -24.0,
            "lon": 0.0,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid put with negative kelvin temperature")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid put with negative kelvin temperature")

        request = self.client.put('http://127.0.0.1:8000/cool_glidings/weatherData/1/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": 15.0,
            "temperature": 24.0,
            "lon": -181,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid put with longitude less than -180")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid put with longitude less than -180")

        request = self.client.put('http://127.0.0.1:8000/cool_glidings/weatherData/1/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": 15.0,
            "temperature": 24.0,
            "lon": 181,
            "lat": 0.0,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid put with longitude more than 180")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid put with longitude more than 180")

        request = self.client.put('http://127.0.0.1:8000/cool_glidings/weatherData/1/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": 15.0,
            "temperature": 24.0,
            "lon": 0.0,
            "lat": -91,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid put with latitude less than -90")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid put with latitude less than -90")

        request = self.client.put('http://127.0.0.1:8000/cool_glidings/weatherData/1/', {
            "date": "2018-08-01",
            "speed": 15,
            "pressure": 10.0,
            "humidity": 15.0,
            "temperature": 24.0,
            "lon": 0.0,
            "lat": 91,
            "time": "09:43:32"
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 after invalid put with latitude more than 90")
        self.assertEqual(len(WeatherData.objects.all(
        )), 1, "New weather data was added despite invalid put with latitude more than 90")

    def test_delete_weather_data(self):
        """
        Tests to make sure the server returns a 204 if the delete request was
        successfull
        """
        request = self.client.delete(
            'http://127.0.0.1:8000/cool_glidings/weatherData/1/')
        self.assertEqual(request.status_code, 204)
