"""
Contains tests for the generator end point in the rest api
"""
from rest_framework.test import APIClient, APITestCase
from cool_glidings.models import Generator
from populate_cool_glidings import populate


class GeneratorAPITests(APITestCase):
    """
    Class containing tests regarding generators
    """
    def setUp(self):
        """
        Set up the environment for the tests, by running the population
        script and creating a dumby client
        """
        try:
            populate()
        except ImportError:
            print("The module population_script does not exist")
        except NameError:
            print("The module populate() does not exist or is not correct")
        except:
            print("Something went wrong in the populate() function")
        self.client = APIClient()

    def test_get_generator(self):
        """
        Make sure the server returns a 200 if the get request
        to retrieve all the generators was successfull
        """
        request = self.client.get(
            'http://127.0.0.1:8000/cool_glidings/generators/')
        data = request.data[0]
        self.assertEqual(request.status_code, 200)
        self.assertEqual(data['label'], "F47C489")
        self.assertEqual(data['powerCoefficient'], 0.5)
        self.assertEqual(data['radius'], 15)
        self.assertEqual(data['height'], 24.0)

    def test_get_generator_specific(self):
        """
        Make sure the server returns a 200 to retrieve a specific
        generator from the database
        """
        request = self.client.get(
            'http://127.0.0.1:8000/cool_glidings/generators/F47C489/')
        data = request.data
        self.assertEqual(request.status_code, 200)
        self.assertEqual(data['label'], "F47C489")
        self.assertEqual(data['powerCoefficient'], 0.5)
        self.assertEqual(data['radius'], 15)
        self.assertEqual(data['height'], 24.0)

    def test_post_generator_valid_returns_201(self):
        """
        Tests to see if the server returns a 201 if there was a
        successfull post request
        """
        request = self.client.post('http://127.0.0.1:8000/cool_glidings/generators/', {
            'powerCoefficient': 0.3,
            'radius': 50,
            'height': 150,
            'label': 'test'
        })
        self.assertEqual(request.status_code, 201,
                         "Did not return 201 for valid data")

        self.assertTrue(Generator.objects.filter(
            label="test").exists(), "Generator was not created in the database")

    def test_post_generator_invalid_returns_400(self):
        """
        Tests to make sure the server returns a 400 if the parameters passed when
        making a post request are invalid
        """
        request = self.client.post('http://127.0.0.1:8000/cool_glidings/generators/', {
            'powerCoefficient': -0.1,
            'radius': 50,
            'height': 150,
            'label': 'test'
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 when powerCoefficient is less than 0")

        request = self.client.post('http://127.0.0.1:8000/cool_glidings/generators/', {
            'powerCoefficient': 1.1,
            'radius': 50,
            'height': 150,
            'label': 'test'
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 when powerCoefficient is greater than 1")

        request = self.client.post('http://127.0.0.1:8000/cool_glidings/generators/', {
            'powerCoefficient': 0.4,
            'radius': -20,
            'height': 150,
            'label': 'test'
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 when radius is not positive")

        request = self.client.post('http://127.0.0.1:8000/cool_glidings/generators/', {
            'powerCoefficient': 0.4,
            'radius': 20,
            'height': -150,
            'label': 'test'
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 when height is not positive")

    def test_put_generator_valid(self):
        """
        Tests to make sure the server returns a 200 if the put request was
        successfull
        """
        request = self.client.put('http://127.0.0.1:8000/cool_glidings/generators/F47C489/', {
            "label": "F47C489",
            "powerCoefficient": 0.5,
            "radius": 20,
            "height": 24.0
        })
        self.assertEqual(request.status_code, 200)

    def test_put_generator_invalid_returns_400(self):
        """
        Tests to make sure the server returns a 400 if the put request contained
        invalid parameters
        """
        request = self.client.put('http://127.0.0.1:8000/cool_glidings/generators/F47C489/', {
            'powerCoefficient': -0.1,
            'radius': 50,
            'height': 150,
            'label': 'test'
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 when powerCoefficient is less than 0")

        request = self.client.put('http://127.0.0.1:8000/cool_glidings/generators/F47C489/', {
            'powerCoefficient': 1.1,
            'radius': 50,
            'height': 150,
            'label': 'test'
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 when powerCoefficient is greater than 1")

        request = self.client.put('http://127.0.0.1:8000/cool_glidings/generators/F47C489/', {
            'powerCoefficient': 0.4,
            'radius': -20,
            'height': 150,
            'label': 'test'
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 when radius is not positive")

        request = self.client.put('http://127.0.0.1:8000/cool_glidings/generators/F47C489/', {
            'powerCoefficient': 0.4,
            'radius': 20,
            'height': -150,
            'label': 'test'
        })
        self.assertEqual(request.status_code, 400,
                         "Did not return 400 when height is not positive")

    def test_delete_generators(self):
        """
        Tests to make sure the server returns a 204 if the delete request was
        successfull and a 404 if the generator does not exist
        """
        request = self.client.delete(
            'http://127.0.0.1:8000/cool_glidings/generators/F47C489/')
        self.assertEqual(request.status_code, 204,
                         "Did not return 204 on valid DELETE request")

        self.assertFalse(Generator.objects.filter(
            label="F47C489").exists(), "The generator was not deleted.")

        request2 = self.client.delete(
            'http://127.0.0.1:8000/cool_glidings/generators/F47C489/')
        self.assertEqual(request2.status_code, 404,
                         "Did not return 404 when generator was not in the database")
