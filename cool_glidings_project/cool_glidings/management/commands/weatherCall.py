"""This module gets and save weather data for all locations"""
import datetime
from django.core.management.base import BaseCommand

from cool_glidings.DataProcessing.helperFunctions.datetimeStuff import changeDateFormat
from cool_glidings.DataProcessing.currentWeather.currentWeatherRetrieval import get_weather_data_now
from cool_glidings.DataProcessing.helperFunctions.unassorted import data_fetch, data_post


class Command(BaseCommand):
    """new Command that gets current weather data"""
    LOCATIONS_LIST_GET_URL = ("http://django-env.izjypmbxc6.eu-west-2.elasticbeanstalk.com"
    "/cool_glidings/locations/")
    WEATHER_DATA_POST_URL = ("http://django-env.izjypmbxc6.eu-west-2.elasticbeanstalk.com"
    "/cool_glidings/weatherData/")

    # (requires the server to be running to run)
    def handle(self, *args, **options):
        """default function to handle the new command,
        calling the static method below with functions using urllib"""
        #pylint: disable=unused-argument
        self.get_and_save_current_weather_for_locations(data_fetch, data_post)

    # Gets and saves the current weather
    # for every location in the database
    # location_fetch - a function that is passed a string url
        # should return a list of locations
    # weather_post - a function that is passed a dictionary of values
        # and a string url that should allow a POST
        # and return a boolean for whether the request succeeded or not

    def get_and_save_current_weather_for_locations(self, location_fetch, weather_post):
        """self-explanatory"""
        # Get all locations
        locations = location_fetch(Command.LOCATIONS_LIST_GET_URL)
        # Check if there are locations
        if locations:
            coord_pairs = []
            for location in locations:
                pair = (location['lat'], location['lon'])
                # Get weather data only if the pair of coordinates has not been processed yet
                if pair not in coord_pairs:
                    coord_pairs.append(pair)
                    # Get the weather API response
                    response = get_weather_data_now(pair[0], pair[1])
                    # If the response has been completed successfully
                    if response is not None:
                        # Change the date format from dd-mm-yyyy to yyyy-mm-dd
                        date_format = changeDateFormat(response['date'])
                        # Get the current time as hh:mm and
                        # set seconds and microsecond to 0
                        current_time = datetime.datetime.now().time().replace(second=0,
                                                                              microsecond=0)
                        # If date has been processed
                        # correctly save the weatherData object
                        if date_format is not False:
                            # Perform the post request
                            status = weather_post({'date': date_format,
                                                   'speed': response['wind_speed'],
                                                   'pressure': response['pressure'],
                                                   'humidity': response['humidity'],
                                                   'temperature': response['temp'],
                                                   'lon': pair[1],
                                                   'lat': pair[0],
                                                   'time': current_time},
                                                  Command.WEATHER_DATA_POST_URL)

                            if status:
                                self.stdout.write(
                                    "WeatherData object created successfully.")
                            else:
                                self.stdout.write(
                                    "ERROR: Something failed when creating the WeatherData object.")
                        else:
                            self.stdout.write(
                                "ERROR: Something failed when formatting the date.")
                    else:
                        self.stdout.write(
                            "ERROR: Something failed when retrieving data from openWeatherMap API")
        else:
            self.stdout.write(
                "ERROR: Something failed when retrieving data from the location API")
