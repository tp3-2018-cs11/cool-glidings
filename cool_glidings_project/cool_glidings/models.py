""" Our database models used to represent data in a object orientated way """

import django
from django.db import models


class Generator(models.Model):
    """ Blueprint of a model instance that represents a wind turbine model """

    powerCoefficient = models.FloatField(null=True, blank=True)
    radius = models.FloatField(null=True, blank=True)
    height = models.FloatField(default=150, blank=True)
    label = models.CharField(max_length=128, unique=True, primary_key=True)

    def __str__(self):
        return "Generator: " + self.label


class WeatherData(models.Model):
    """ Blueprint of a model instance that represents a singular point of weather data """

    _id = models.AutoField(primary_key=True)
    date = models.DateField(
        default=django.utils.timezone.now, null=True, blank=True)
    speed = models.FloatField(null=True, blank=True)
    pressure = models.FloatField(null=True, blank=True)
    humidity = models.FloatField(null=True, blank=True)
    temperature = models.FloatField(null=True, blank=True)
    lon = models.FloatField(default=0.0, null=True, blank=True)
    lat = models.FloatField(default=0.0, null=True, blank=True)
    time = models.TimeField(
        default=django.utils.timezone.now, null=True, blank=True)

    def __str__(self):
        return "Weatherdata: " + "{0}".format(self._id)

    class Meta:
        ordering = ('date', 'time')


class Location(models.Model):
    """ Blueprint of a model instance that represents a location in the world """

    name = models.CharField(max_length=128, unique=True, primary_key=True)
    lat = models.FloatField(default=0.0, null=True, blank=True)
    lon = models.FloatField(default=0.0, null=True, blank=True)

    def __str__(self):
        return "Location: " + self.name
