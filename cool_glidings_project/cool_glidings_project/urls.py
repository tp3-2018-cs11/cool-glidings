"""
Contains the URLs for each app within the django project
"""
from django.views.generic.base import RedirectView
from django.contrib import admin
from django.urls import path
from django.conf.urls import include

urlpatterns = [
    # simple redirect to the Django Rest Framework locations list view
    path('', RedirectView.as_view(url="/cool_glidings/locations/", permanent=False)),
    path('cool_glidings/', include('cool_glidings.urls')),
    path('admin/', admin.site.urls),
]
