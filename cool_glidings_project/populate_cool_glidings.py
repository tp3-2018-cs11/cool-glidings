""" Population script to load basic made up data into the database """
import datetime
import pytz
from cool_glidings.models import Generator, WeatherData, Location


def populate():
    """ Populates the database with 1 of each model
    """

    generators = [
        {"label": "F47C489", "powerCoefficient": 0.5, "radius": 15, "height": 24.0},
    ]

    weather_data = [
        {"date": datetime.datetime(2014, 8, 18, 6, 50, 41, 820000, tzinfo=pytz.timezone('UTC')),
         "speed": 15,
         "pressure": 10.0,
         "humidity": 15.0,
         "temperature": 24.0,
         "lon": 0.0,
         "lat": 0.0,
         "time": datetime.time(9, 34, 32)
         }
    ]

    location = [
        {"name": "Carcant Wind Farm", "lat": 55.7660, "lon": 3.0189}
    ]

    location = [
        {"name": "Carcant Wind Farm", "lat": 55.7660, "lon": 3.0189}
    ]

    for generator_data in generators:
        gen = add_generator(
            generator_data["label"],
            generator_data["powerCoefficient"],
            generator_data["radius"],
            generator_data["height"])

    # terminal print statements when populating
    print("Populating generators ...")
    for gen in Generator.objects.filter(label=generators[0]['label']):
        print("- {0}".format(str(gen)))

    for weather_point in weather_data:
        weather = add_weatherdata(
            weather_point["date"],
            weather_point["speed"],
            weather_point["pressure"],
            weather_point["humidity"],
            weather_point["temperature"],
            weather_point["lon"],
            weather_point["lat"],
            weather_point["time"]
        )

    # terminal print statements when populating
    print("Populating weather data ...")
    for weather in WeatherData.objects.filter(date=weather_data[0]['date']):
        print("- {0}".format(str(weather)))

    for loc in location:
        loc = add_location(
            loc["name"],
            loc["lat"],
            loc["lon"]
        )
    # terminal print statements when populating
    print("Populating location data ...")
    for loc in Location.objects.filter(name=location[0]['name']):
        print("-{0}".format(str(loc)))


def add_generator(label, power_coefficient, radius, height):
    """ Adds a generator to the database with the given parameters

    Arguments:
        label {string} -- the name of the generator
        power_coefficient {float} -- a value between 0 & 1 (inclusive) representing power gain
        radius {float} -- a non negative number that represents the turbines blade length
        height {float} -- a non negative number representing the height of the turbine

    Returns:
        Generator Model -- An instance of the generator model
    """
    gen = Generator.objects.get_or_create(label=label)[0]
    gen.powerCoefficient = power_coefficient
    gen.radius = radius
    gen.height = height
    gen.save()
    return gen


def add_weatherdata(date, speed, pressure, humidity, temperature, lon, lat, time):
    """ Creates an instance of a weather data model in the database

    Arguments:
        date {datetime.datetime} -- date of the weather data occurrence
        speed {float} -- wind speed in m/s
        pressure {float} -- air pressure in hPa
        humidity {float} -- humidity % of air
        temperature {float} -- non-negative kelvin value
        lon {float} -- longitude
        lat {float} -- latitude
        time {datetime.time} -- time of the weather occurrence

    Returns:
        Weather -- weather data model instance
    """
    weather = WeatherData.objects.get_or_create(date=date)[0]
    print(weather)
    weather.speed = speed
    weather.pressure = pressure
    weather.humidity = humidity
    weather.temperature = temperature
    weather.lon = lon
    weather.lat = lat
    weather.time = time
    weather.save()
    return weather


def add_location(name, lat, lon):
    """ Creates a location instance in the database

    Arguments:
        name {string} -- name of the location
        lat {float} -- latitude
        lon {float} -- longitude

    Returns:
        Location -- model instance created
    """
    loc = Location.objects.get_or_create(name=name)[0]
    loc.lat = lat
    loc.lon = lon
    loc.save()
    return loc
