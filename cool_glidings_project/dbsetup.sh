#!/bin/bash

python3 manage.py makemigrations cool_glidings
python3 manage.py migrate
python3 manage.py loaddata data.json

exit 0
