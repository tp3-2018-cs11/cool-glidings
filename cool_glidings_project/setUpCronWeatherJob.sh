#!/bin/bash

# This bash script finds a full path to the python file to run and puts it in as a cron job for the user.

# Getting the working directory path (this should always be the root of the project with gitlab runner)
dirName="$(pwd | sed 's/ /\\ /g')"

# Concatenating the end of the path to the manage.py script that is to be run every 15 minutes
fullPath="$dirName/manage.py"

# echo both of these to check that everything went well
echo $dirName
echo $fullPath

# removing the previous cron jobs for the user (this is here because each push causes a new build to be downloaded and we don't want lots of jobs going at the same time)
crontab -r

# Get all unique entries and put them into the cron job config file
(crontab -l ; echo "*/15 * * * * python3 $fullPath weatherCall" ; printf "\n") | sort -r - | uniq - | crontab -
