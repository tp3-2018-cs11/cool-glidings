# How to Setup and Use Cool Glidings

This is a small guide over how you can deploy the Cool Glidings project locally.

## Prerequisites

If you haven't already, you should install the prerequisites as stated in the README.

### Python Virtual Environment

It is highly recommended that you create a virtual environment to run the code in as this will keep all your python dependencies contained and easy to manage.

To create a virtual environment execute: `python -m venv your_environment_name`

Then to enter the environment you should execute: `source your_environment_name/bin/activate`

## Setup

There is a script for each part of setting up Cool Glidings:
- Setup
- Build
- Test
- Run

Luckily, we have a **deploy** script that encompasses all of these in it. So run the deploy script to get set up. :)

### Later Use

Obviously for later use you can simply run whichever script you shall need e.g. the setup script to update the packages used by the project or the run script to run and see the webapp.

## Backend Database

The database used to store all of our data is an SQLite database that is accessed by the django backend using a Object Relational Mapping. The database is just a .sqlite file and you can dump and load the database data by using the methods described in the [Django Documentation](https://docs.djangoproject.com/en/2.1/ref/django-admin/#dumpdata).

Alternatively if you wish to use it in MySQL or something similar you would need to make a .sql conversion of the database file (that can not act as a replacement for the backend database) and then use that database file.