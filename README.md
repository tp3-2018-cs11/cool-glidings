# Cool Glidings

## Project Overview
The project, developed for our Level 3 Computer Science Team Project, consists of a web application that allows users to query energy production of wind turbines (across the UK) over select periods of time. The result of the queries is then visualised and can be used for further analysis. 

The core system allows users to

- add their own generators to use, as well as modify ones that are already stored in the database. 
- create and manage locations. 
- Manually insert weather data in the database for specific locations.

The end product is a web app made of 2 parts: 

- **Django Backend:** Using the Django rest framework we created a Django REST API backend that retrieves and saves weather data, allows CRUD for generators and locations, as well as allowing for energy queries.

- **Angular Frontend:** Using Angular we have a frontend UI that works in tandem with the Django backend to give a smooth user experience.

This project was defined by and overseen by **Smarter Grid Solutions**.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
Here are listed the prerequisites needed in order to set up and run the project. It is recommended to install the prerequisites in the following order.  
#### Python
The latest version of python can be downloaded from [Python.org](https://www.python.org/downloads/)
#### Pip
Pip can be installed by following the instructions at [Pip.pypa.io](https://pip.pypa.io/en/stable/installing/) 

#### Node.js
The latest versions of Node.js can be downloaded from [Nodejs.org](https://nodejs.org/en/)

#### Angular
Angular can be installed by executing the command below. 

```
npm install -g @angular/cli
```
For additional information about the Angular installation check the official page [Angular.io](https://angular.io/guide/quickstart) 

### Installing

To install all required dependencies execute the following command from the main project directory. 

```
./setup.sh
```
### Build the project
To build the project execute the following command from the main project directory. 

```
./buildproject.sh
```

### Run the project
To run the project execute the following command from the main project directory. 

```
./runservers.sh
```

## Execute tests
Tests have been implemented for both front-end (Angular) and back-end (Django).
To run the tests the following command from the main project directory. 

```
./testproject.sh
``` 
## Deployment
To deploy the project execute the following command from the main project directory. 

```
./deploy.sh
```

## Built With
- [Django](https://www.djangoproject.com/): Used to build the web app back-end structure
- [Django Rest Framework](https://www.django-rest-framework.org/): Used to build the back-end API
- [Angular](https://angular.io/): Used for the front-end implementation. 
- [Ant Design](https://ng.ant.design/docs/introduce/en): Used for the front-end design. 

## Authors

- Project Manager - Matthew
- Customer Liaison - Alessandro
- Tool Smith - Pierre
- Test Manager - Ruairidh
- Librarian - Morgan


## License
This project is licensed under the [MIT License](https://choosealicense.com/licenses/mit/)
