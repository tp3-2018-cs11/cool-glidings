# How to Use the Rest API

## Overview

The rest API is implemented using django-rest-frameworks. There are 3 models in the database:
- Generator - Can make full CRUD operations
- Locations - Can make full CRUD operations
- Weather Data - Can only make post requests too

There is also a rest end point for the enegy query, only a GET request can be made to this.


## URLs

The base URL will be: http://127.0.0.1:8000/cool_glidings/<br>
Or<br>
AWS Server: http://django-env.izjypmbxc6.eu-west-2.elasticbeanstalk.com/cool_glidings/<br>

To make a http request to the API, the urls are the following:
- < hostname >/cool_glidings/generators/
- < hostname >/cool_glidings/locations/
- < hostname >/cool_glidings/weatherData/
- < hostname >/cool_glidings/energyQuery/

In order to get access a specific generator/location, append the PK of the object to the end of the url as follows:<br>
- < hostname >/cool_glidings/generators/< label >/
- < hostname >/cool_glidings/location/< name >/

## Format of Data

The server will return a http error code (400) if the data is not in the following format for the resepective model.

### Generator

The fields are:
- powerCoefficiant (float) - Must have a value between 0 and 1 (inclusive) 
- radius (float) - Must be greater than 0
- height (float) - must be greater than 0
- label (string) - Must be unique and not null (this is the primary key for generator)

### Weather Data

The fields are:
- date (datefield) - Must be enetered in the format 'YYYY-MM-DD'
- speed (float) - Must be greater than 0
- pressure (float) - Must be greater than 0
- humidity (float) - Must be between 0 and 100 (inclusive)
- lon (float) - Must be between -180 and 180 (inclusive)
- lat (float) - Must be between -90 and 90 (inclusive)
- The primary key of this model is an integer assigned to it upon creation and isn't required

### Location

The fields are:
- name (string) - Must be unique and not null (this is the primary key for location)
- lon (float) - Must be between -180 and 180 (inclusive)
- lat (float) - Must be between -90 and 90 (inclusive)

### Energy Query

The fields are:
- loc (string) - location name (compulsory)
- gen (string) - generator label (compulsory)
- startDate (date field) - start date (optional), must be in format 'YYYY-MM-DD'
- endDate (date field) - end date (optional), must be in format 'YYYY-MM-DD'
- startTime (time field) - starting time (optional), must be in format 'HH:MM:SS'
- endTime (time field) - end time (optional), must be in format 'HH:MM:SS'
- format (string) - can be either 'application/json' or 'csv' ('application/json' is the default and does not have to be specified)

## Return values

A GET request to (all return 200 if successfull, 404 if not found):
- /locations/ - Returns all the locations in the database
- /locations/< name >/ - Returns the location with name 'name'
- /generators/ - Returns all the generators in the database
- /generators/< label >/ - Returns the generator label 'label'
- /energyQuery/?loc=< loc >&gen=< gen >&startDate=< startDate >&endDate=< endDate >&startTime=< startTime >&endTime=< endTime >&format=< format >
  - Returns an array of energy values from within the range specified in the parameters and with the specific generator
  - Returns a 422 if there is no weather data in the specified range and hence no array of energy values will be returned

A POST request to (all returns a 201 if successfull, 400 if invalid input):
- /locations/?name=< name >&lon=< lon >&lat = < lat >/
- /generators/?label=< label >&radius=< radius >&height=< height >&powerCoefficiant=< powerCoefficiant >/
- /weatherData/?date=< date >&speed=< speed >&pressure=< pressure >&humidity=< humidity >&lon=< lon >&lat=< lat >/

A PUT request to (all return 200 if successfull, 400 if invalid input, 404 if not found):
- /locations/< name >/
- /generators/< label >/

A DELETE request to (all return a 204 if successfull, 404 if not found):
- /locations/< name >/
- /generators/< label >/
