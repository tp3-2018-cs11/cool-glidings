#!/bin/bash

# Runs the database script, which creates it and runs a population script
cd ../cool_glidings_project
./dbsetup.sh

# Builds the angular project
cd ../cool-glidings-frontend
npm run build

exit 0