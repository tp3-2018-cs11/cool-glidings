#!/bin/bash
# This bash script is used in order to simulate CI locally.

cd ../

# Django
#--------------------------------------------------------
# Initial environment setup
pip3 install -r requirements.txt
#--------------------------------------------------------
# Test to execute
coverage run --source=cool_glidings_project/cool_glidings/DataProcessing/ -m pytest cool_glidings_project/cool_glidings/DataProcessing
coverage html -d dataProcessingCov
coverage run --source=cool_glidings_project/ --omit=cool_glidings_project/cool_glidings/DataProcessing/* cool_glidings_project/manage.py test cool_glidings
coverage html -d djangoCov
#--------------------------------------------------------

# Angular
#--------------------------------------------------------
# Initial Environment Setup
cd cool-glidings-frontend
npm install -D
#--------------------------------------------------------
# Testing
npm run test -- --no-watch --no-progress --browsers=ChromeHeadlessCI --code-coverage
npm run e2e -- --protractor-config=e2e/protractor-ci.conf.js
#--------------------------------------------------------
# Building
#--------------------------------------------------------
npm run build
#--------------------------------------------------------

