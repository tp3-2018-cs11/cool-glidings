#!/bin/bash

cd ../

# Tests the Data Processing python files and displays code coverage
coverage run --source=cool_glidings_project/cool_glidings/DataProcessing/ -m pytest cool_glidings_project/cool_glidings/DataProcessing
coverage report

# Tests the djagno project
coverage run --source=cool_glidings_project/ --omit=cool_glidings_project/cool_glidings/DataProcessing/* cool_glidings_project/manage.py test cool_glidings
coverage report

# Tests the Angular project
cd cool-glidings-frontend
npm run test -- --no-watch --no-progress --browsers=ChromeHeadlessCI --code-coverage
npm run e2e -- --protractor-config=e2e/protractor-ci.conf.js

# Check if the -r flag has been passed, if so then delete all
# files/directories produced when running the tests
if [ "$1" == "-r" ]; then
    rm -r coverage/
    cd ..
    rm .coverage
    rm -r .pytest_cache/
    cd cool_glidings_project/
    rm -r __pycache__/
    rm -r cool_glidings_project/__pycache__/
    rm -r cool_glidings/__pycache__/
    rm -r cool_glidings/migrations/__pycache__/
    rm -r cool_glidings/management/commands/__pycache__/
    rm -r cool_glidings/views/__pycache__/
    rm -r cool_glidings/tests/__pycache__/
    cd cool_glidings/DataProcessing
    rm -r __pycache__/
    for arg in $(ls); do
        if [ -d ${arg} ]; then
            rm -r ${arg}/__pycache__/
        fi
    done
fi

exit 0
