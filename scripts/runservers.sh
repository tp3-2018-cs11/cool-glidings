#!/bin/bash

cd ../

# kill any processes on port 8000 and 4200
npx kill-port 4200
npx kill-port 8000

cd cool-glidings-frontend
python3 ../cool_glidings_project/manage.py runserver & ng serve -o

exit 0