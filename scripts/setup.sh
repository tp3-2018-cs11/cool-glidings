#!/bin/bash

cd ../

# Python dependencies
pip3 install -r requirements.txt

# Angular dependencies
cd cool-glidings-frontend
npm install -D

exit 0