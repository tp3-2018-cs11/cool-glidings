import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationsPageComponent } from './locations-page.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormGroup, FormControl } from '@angular/forms';

describe('LocationsPageComponent', () => {
  let component: LocationsPageComponent;
  let fixture: ComponentFixture<LocationsPageComponent>;
  let htmlElement: HTMLElement;
  let goodForm: any;
  let badForm: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationsPageComponent ],
      imports: [
        ReactiveFormsModule,
        NgZorroAntdModule,
        HttpClientModule,
        BrowserAnimationsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationsPageComponent);
    component = fixture.componentInstance;
    htmlElement = fixture.debugElement.nativeElement;
    component.ngOnInit();
    goodForm = {
      'name': 'My house',
      'lon': '180',
      'lat': '-90'
    };
    badForm = {
      'name': ' ',
      'lon': '182',
      'lat': '-91'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have an h2 tag "Manage your Locations"', async(() => {
    expect(htmlElement.querySelector('h2').textContent).toEqual('Manage your Locations');
  }));

  it('should have editLcation form group', () => {
    expect(component.editLocationForm instanceof FormGroup).toBe(true);
  });

  it('edit Location should contain all fields', () => {
    expect(component.editLocationForm.controls['name']).toBeDefined();
    expect(component.editLocationForm.controls['lon']).toBeDefined();
    expect(component.editLocationForm.controls['lat']).toBeDefined();
    expect(component.editLocationForm.controls['undefined']).toBeUndefined();
  });

  it('should have create Location form group', () => {
    expect(component.createLocationForm instanceof FormGroup).toBe(true);
  });

  it('create Location should contain all fields', () => {
    expect(component.createLocationForm.controls['name']).toBeDefined();
    expect(component.createLocationForm.controls['lon']).toBeDefined();
    expect(component.createLocationForm.controls['lat']).toBeDefined();
    expect(component.createLocationForm.controls['undefined']).toBeUndefined();
  });

  it('Should delete a location correctly', () => {
    component.data = [...component.data, goodForm];
    component.editLocationForm.setValue(goodForm);
    component.createLocationForm.setValue(goodForm);
    component.delete(goodForm.name);

    expect(component.data.filter(obj => obj.name === goodForm.name).length === 0).toBeTruthy();
    expect(component.editLocationForm.valid).toBeFalsy();
    expect(component.createLocationForm.valid).toBeTruthy();
  });

  it('Should create a location correctly', () => {
    component.createLocationForm.setValue(goodForm);
    component.editLocationForm.setValue(goodForm);

    expect(component.createLocationForm.valid).toBeTruthy();

    component.createLocation();

    expect(component.data.filter(obj => obj.name === goodForm.name).length === 1).toBeTruthy();
    expect(component.createLocationForm.pristine).toBeTruthy();
    expect(component.createLocationForm.untouched).toBeTruthy();
    expect(component.editLocationForm.valid).toBeTruthy();
  });

  it('Should not create a location if createLocationForm is invalid', () => {
    component.createLocationForm.setValue(badForm);
    component.createLocation();

    expect(component.data.filter(obj => obj.name === goodForm.name).length === 0).toBeTruthy();
    expect(component.createLocationForm.valid).toBeFalsy();
  });

  it('Should edit a location correctly', () => {
    component.data = [...component.data, goodForm];
    const exampleForm = {
      'name': goodForm.name,
      'lon': 55,
      'lat': 50
    };
    component.editLocationForm.setValue(exampleForm);

    expect(component.editLocationForm.valid).toBeTruthy();

    component.editLocation();
    expect(component.data.find(obj => obj.name === goodForm.name).lon).toBe(55);
    expect(component.data.find(obj => obj.name === goodForm.name).lat).toBe(50);
    expect(component.editLocationForm.pristine).toBeTruthy();
    expect(component.editLocationForm.untouched).toBeTruthy();
  });

  it('Should not edit a location if editLocationForm is invalid', () => {
    component.editLocationForm.setValue(badForm);
    component.editLocation();
    expect(component.editLocationForm.valid).toBeFalsy();
  });

  it('Should populate the edit form correctly if the location exists', () => {
    component.data = [...component.data, goodForm];
    component.editLocationForm.get('name').setValue(goodForm.name);

    component.selectChange(goodForm.name);
    expect(component.editLocationForm.get('lon').value).toBe(goodForm.lon);
    expect(component.editLocationForm.get('lat').value).toBe(goodForm.lat);
    expect(component.editLocationForm.valid).toBeTruthy();
  });

  it('Test validLocationName: Should return true(error) if the location already exists', () => {
    component.data = [...component.data, goodForm];
    expect(component.validLocationName(new FormControl(goodForm.name)).error).toBeTruthy();
  });

  it('Test validLocationName: Should return undefined(success) if the location does not exists', () => {
    expect(component.validLocationName(new FormControl(goodForm.name))).toBe(undefined);
  });

  it('Test validLocationNameExists: Should return undefined(success) if the location already exists', () => {
    component.data = [...component.data, goodForm];
    expect(component.validLocationNameExists(new FormControl(goodForm.name))).toBe(undefined);
  });

  it('Test validLocationNameExists: Should return true(error) if the location does not exists', () => {
    expect(component.validLocationNameExists(new FormControl(goodForm.name)).error).toBeTruthy();
  });

  it('Should sort data ascending', () => {
    const newForm = {
      'name': 'Aaa',
      'lon': '50',
      'lat': '60'
    };

    component.data = [...component.data, goodForm];
    component.data = [...component.data, newForm];
    component.columnSort({'value': 'ascend', 'key': 'name'});

    expect(component.data.findIndex(obj => obj.name === newForm.name)).toBe(0);
    expect(component.data.findIndex(obj => obj.name === goodForm.name)).toBe(1);
  });

  it('Should sort data descending', () => {
    const newForm = {
      'name': 'Aaa',
      'lon': '50',
      'lat': '60'
    };

    component.data = [...component.data, goodForm];
    component.data = [...component.data, newForm];
    component.columnSort({'value': 'descend', 'key': 'name'});

    expect(component.data.findIndex(obj => obj.name === newForm.name)).toBe(1);
    expect(component.data.findIndex(obj => obj.name === goodForm.name)).toBe(0);
  });

});
