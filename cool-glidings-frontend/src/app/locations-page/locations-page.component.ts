import { Component, OnInit } from '@angular/core';
import { ApiCallsService } from './../Services/api/api-calls.service';
import { NotificationService } from './../Services/notification/notification.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { validateForm, validLatitude, validLongitude, validateWhitespace } from '../validators/validators';

@Component({
  selector: 'app-locations-page',
  templateUrl: './locations-page.component.html',
  styleUrls: ['./locations-page.component.less']
})
export class LocationsPageComponent implements OnInit {

  data: any[] = new Array();
  createLocationForm: FormGroup;
  editLocationForm: FormGroup;

  constructor(
    private api: ApiCallsService,
    private fb: FormBuilder,
    private notification: NotificationService) { }

  ngOnInit() {
    this.getLocations();

    // Initialise the createLocationForm and set its validators
    this.createLocationForm = this.fb.group({
      name : [ null, [ Validators.required, this.validLocationName, validateWhitespace] ],
      lat : [ null, [ Validators.required, validLatitude] ],
      lon : [ null, [ Validators.required, validLongitude] ]
    });

    // Initialise the editLocation form and set its validators
    this.editLocationForm = this.fb.group({
      name : [ null, [ Validators.required, validateWhitespace, this.validLocationNameExists] ],
      lat : [ null, [ Validators.required, validLatitude] ],
      lon : [ null, [ Validators.required, validLongitude] ]
    });
  }

  /**
   * Returns an array of locations called from the server
   */
  getLocations() {
    this.api.get('locations').subscribe(data => this.data = data);
  }

  /**
   * Deletes an element from both the data set and from the server
   */
  delete(key: string) {
    this.api.delete('locations', key).subscribe();
    this.data = this.data.filter(object => object.name !== key);
    this.notification.createNotification('success', 'Location Deleted successfully');
    if (this.editLocationForm.get('name').value !== null) {
      validateForm(this.editLocationForm);
    }
    if (this.createLocationForm.get('name').value !== null) {
      validateForm(this.createLocationForm);
    }
  }


  /**
   * ======= CREATE LOCATION FORM ===================================
   * Function executed when the createForm is submitted
   */
  createLocation() {
    // Check that all fields are valid
    validateForm(this.createLocationForm);

    // If the form is valid, create a new location and update the table
    if (this.createLocationForm.valid) {
      this.api.post('locations', this.createLocationForm.value).subscribe();
      this.data = [ ...this.data, this.createLocationForm.value];
      this.createLocationForm.reset();
      this.notification.createNotification('success', 'Location Added Successfully');
      if (this.editLocationForm.get('name').value !== null) {
        validateForm(this.editLocationForm);
      }
    }
  }
  // ====================================================================

  /**
   * ====================== EDIT LOCATION FROM ==========================
   */
  // Function executed when the createForm is submitted
  editLocation() {
    // Check that all inputs are valid
    validateForm(this.editLocationForm);

    // If the location is valid change it in both table and server
    if (this.editLocationForm.valid) {
      this.api.put('locations', this.editLocationForm.value.name, this.editLocationForm.value).subscribe();
      const index = this.data.findIndex(x => x.name === this.editLocationForm.value.name);
      this.data[index].lat = this.editLocationForm.value.lat;
      this.data[index].lon = this.editLocationForm.value.lon;
      this.editLocationForm.reset();
      this.notification.createNotification('success', 'Location Modified Successfully');
    }
  }
  // Function used to change populate the edit form once a location
  // has been selected
  selectChange(key: string) {
    if (this.editLocationForm.get('name').value !== null) {
      this.editLocationForm.get('lat').setValue(this.data.find(loc => loc.name === key).lat);
      this.editLocationForm.get('lon').setValue(this.data.find(loc => loc.name === key).lon);
      validateForm(this.editLocationForm);
    }
  }
  // ====================================================================
  // CUSTOM VALIDATORS FOR THE COMPONENT

  // Check if location name does not exist. It returns true(error) if the name
  // exists in our dataset
  validLocationName = (control: FormControl): { [ s: string ]: boolean } => {
    if (this.data.findIndex(x => x.name === control.value) !== -1) {
      return {error: true};
    }
  }

  // Check if location name exist. It returns true(error) if the name does
  // not exists in our dataset
  validLocationNameExists = (control: FormControl): { [ s: string ]: boolean } => {
    if (this.data.findIndex(x => x.name === control.value) === -1) {
      return {error: true};
    }
}

  // =========================================================================

  /**
   * General function that can be used in order to sort table columns
   * It need an external boolean component's field to toggle the sorting
   * order.
   * It also takes a key, which is the attribute that we are using in order
   * to sort our data set. The key is provided by the $event parameter in
   * the template
   */
  columnSort(event: any) {
    this.data = [...this.data.sort((value1, value2) => {
        if (event.value === 'ascend') {
          return value1[event.key] < value2[event.key] ? -1 : 1;
        } else if (event.value === 'descend') {
          return value1[event.key] > value2[event.key] ? -1 : 1;
        }
     })];
  }
}
