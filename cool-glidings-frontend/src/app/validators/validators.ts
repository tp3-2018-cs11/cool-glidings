import { FormControl, FormGroup } from '@angular/forms';


// Check if power coefficient is valid
export function validPowerCoefficient(control: FormControl | null) {
  if (isNaN(control.value) || control.value < 0 || control.value > 1) {
    return {error: true};
  }
  return null;
}

// Check if value is positive and and a number
export function validPositiveValue(control: FormControl | null) {
  if (isNaN(control.value) || control.value <= 0) {
    return {error: true};
  }
  return null;
}

// Check that the input field does not contain only spaces
export function validateWhitespace(control: FormControl | null) {
  if ((control.value || '').trim().length === 0) {
    return {error: true};
  }
  return null;
}

// Check if latitude is valid
export function validLatitude(control: FormControl | null) {
  if (isNaN(control.value) || control.value < -90 || control.value > 90) {
    return {error: true};
  }
  return null;
}

// Check if longitude is valid
export function validLongitude(control: FormControl | null) {
  if (isNaN(control.value) || control.value < -180 || control.value > 180) {
    return {error: true};
  }
  return null;
}

/**
 * returns true or false depending on wether the value is positive (zero included)
 */
export function validatePositiveValueWithZero(control: FormControl | null) {
  if (isNaN(control.value) || control.value < 0) {
    return {error: true};
  }
  return null;
}

/**
 * returns true or false depedning on wether the humidity was valid
 */
export function validateHumidity(control: FormControl | null) {
  if (isNaN(control.value) || control.value < 0 || control.value > 100) {
    return {error: true};
  }
  return null;
}

  /**
   * returns true if the date is valid
   */
  export function validateDate(control: FormControl | null) {
    if (control.value === '' || control.value === null || control.value === undefined) {
      return {error: true};
    }
    return null;
  }

  /**
   * returns true if the date is valid
   */
  export function validateTime(control: FormControl | null) {
    if (control.value === '' || control.value === null || control.value === undefined) {
      return {error: true};
    }
    return null;
  }

/**
* Validates all input fields of a form
*/
export function validateForm(form: FormGroup) {
  for (const i of Object.keys(form.controls)) {
    form.controls[i].markAsDirty();
    form.controls[i].updateValueAndValidity();
  }
}
