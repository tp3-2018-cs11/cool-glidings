import {
  validPowerCoefficient,
  validPositiveValue,
  validateWhitespace,
  validLatitude,
  validLongitude,
  validatePositiveValueWithZero,
  validateHumidity,
  validateDate,
  validateTime,
  validateForm } from './validators';
import { FormControl, FormBuilder, Validators } from '@angular/forms';

describe('Test powerCoefficient validator', () => {

  it('Should return null(success) if the power coefficient is valid ', () => {
    expect(validPowerCoefficient(new FormControl(0.5))).toBe(null);
  });

  it('Should return true(error) if the power coefficient is NaN', () => {
    expect(validPowerCoefficient(new FormControl('test')).error).toBe(true);
  });

  it('Should return true(error) if the power coefficient is less than 0', () => {
    expect(validPowerCoefficient(new FormControl(-1)).error).toBe(true);
  });

  it('Should return true(error) if the power coefficient is grater than 1', () => {
    expect(validPowerCoefficient(new FormControl(1.5)).error).toBe(true);
  });

});

describe('Test validPositiveValue validator', () => {

  it('Should return null(success) if the value is positive', () => {
    expect(validPositiveValue(new FormControl(5))).toBe(null);
  });

  it('Should return true(error) if value is NaN', () => {
    expect(validPositiveValue(new FormControl('test')).error).toBe(true);
  });

  it('Should return true(error) if the value is less than 0', () => {
    expect(validPositiveValue(new FormControl(-1)).error).toBe(true);
  });

});

describe('Test validateWhitespace validator', () => {

  it('Should return null(success) if the value is not empty', () => {
    expect(validateWhitespace(new FormControl('test'))).toBe(null);
  });

  it('Should return true(error) if value is empty', () => {
    expect(validateWhitespace(new FormControl('')).error).toBe(true);
  });

  it('Should return true(error) if the value consists only of white spaces', () => {
    expect(validateWhitespace(new FormControl('   ')).error).toBe(true);
  });

});

describe('Test validLatitude validator', () => {

  it('Should return null(success) if the value valid', () => {
    expect(validLatitude(new FormControl(45))).toBe(null);
  });

  it('Should return true(error) if value is NaN', () => {
    expect(validLatitude(new FormControl('test')).error).toBe(true);
  });

  it('Should return true(error) if the value is greater than 90', () => {
    expect(validLatitude(new FormControl(91)).error).toBe(true);
  });

  it('Should return true(error) if the value is less than -90', () => {
    expect(validLatitude(new FormControl(-91)).error).toBe(true);
  });

});

describe('Test validLongitude validator', () => {

  it('Should return null(success) if the value valid', () => {
    expect(validLongitude(new FormControl(45))).toBe(null);
  });

  it('Should return true(error) if value is NaN', () => {
    expect(validLongitude(new FormControl('test')).error).toBe(true);
  });

  it('Should return true(error) if the value is greater than 180', () => {
    expect(validLongitude(new FormControl(181)).error).toBe(true);
  });

  it('Should return true(error) if the value is less than -180', () => {
    expect(validLongitude(new FormControl(-181)).error).toBe(true);
  });

});

describe('Test validatePositiveValueWithZero validator', () => {

  it('Should return null(success) if the value is positive', () => {
    expect(validatePositiveValueWithZero(new FormControl(5))).toBe(null);
  });

  it('Should return null(success) if the value is 0', () => {
    expect(validatePositiveValueWithZero(new FormControl(0))).toBe(null);
  });

  it('Should return true(error) if value is NaN', () => {
    expect(validatePositiveValueWithZero(new FormControl('test')).error).toBe(true);
  });

  it('Should return true(error) if the value is less than 0', () => {
    expect(validatePositiveValueWithZero(new FormControl(-1)).error).toBe(true);
  });

});


describe('Test validateHumidity validator', () => {

  it('Should return null(success) if the value is valid', () => {
    expect(validateHumidity(new FormControl(5))).toBe(null);
  });

  it('Should return true(error) if value is NaN', () => {
    expect(validateHumidity(new FormControl('test')).error).toBe(true);
  });

  it('Should return true(error) if the value is less than 0', () => {
    expect(validateHumidity(new FormControl(-1)).error).toBe(true);
  });

  it('Should return true(error) if the value is greater than 100', () => {
    expect(validateHumidity(new FormControl(101)).error).toBe(true);
  });

});

describe('Test validateDate validator', () => {

  it('Should return null(success) if the value is valid', () => {
    expect(validateDate(new FormControl('10/11/2012'))).toBe(null);
  });

  it('Should return true(error) if value is an empty string', () => {
    expect(validateDate(new FormControl('')).error).toBe(true);
  });

  it('Should return true(error) if the value is null', () => {
    expect(validateDate(new FormControl(null)).error).toBe(true);
  });

  it('Should return true(error) if the value is undefined', () => {
    expect(validateDate(new FormControl(undefined)).error).toBe(true);
  });

});

describe('Test validateTime validator', () => {

  it('Should return null(success) if the value is valid', () => {
    expect(validateTime(new FormControl('10:24'))).toBe(null);
  });

  it('Should return true(error) if value is an empty string', () => {
    expect(validateTime(new FormControl('')).error).toBe(true);
  });

  it('Should return true(error) if the value is null', () => {
    expect(validateTime(new FormControl(null)).error).toBe(true);
  });

  it('Should return true(error) if the value is undefined', () => {
    expect(validateTime(new FormControl(undefined)).error).toBe(true);
  });

});

describe('Test validateForm', () => {
  const testBuilder: FormBuilder = new FormBuilder();

  it('Should be true(errors) of the form is empty', () => {
    const testForm = testBuilder.group({
      field1 : [ null, [ Validators.required] ],
      field2 : [ null, [ Validators.required] ]
    });

    validateForm(testForm);
    expect(testForm.dirty).toBe(true);
    expect(testForm.valid).toBe(false);
  });

  it('Should be false(valid) of the form is not empty', () => {
    const testForm = testBuilder.group({
      field1 : [ 'Test', [ Validators.required] ],
      field2 : [ 'Test', [ Validators.required] ]
    });

    validateForm(testForm);
    expect(testForm.dirty).toBe(true);
    expect(testForm.valid).toBe(true);
  });

});

