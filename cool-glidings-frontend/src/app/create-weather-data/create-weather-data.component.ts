import { NotificationService } from './../Services/notification/notification.service';
import { ApiCallsService } from './../Services/api/api-calls.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import {
  validatePositiveValueWithZero,
  validateHumidity,
  validLongitude,
  validLatitude,
  validateTime,
  validateForm } from '../validators/validators';

@Component({
  selector: 'app-create-weather-data',
  templateUrl: './create-weather-data.component.html',
  styleUrls: ['./create-weather-data.component.less']
})
export class CreateWeatherDataComponent implements OnInit {
  public createForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private api: ApiCallsService,
    private notification: NotificationService) {
    this.createForm = this.fb.group({
      date: [null, [Validators.required]],
      speed: [null, [Validators.required, validatePositiveValueWithZero]],
      pressure: [null, [Validators.required, validatePositiveValueWithZero]],
      humidity: [null, [Validators.required, validateHumidity]],
      temperature: [null, [Validators.required, validatePositiveValueWithZero]],
      lon: [null, [Validators.required, validLongitude]],
      lat: [null, [Validators.required, validLatitude]],
      time: [null, [Validators.required, validateTime]]
    });
  }


  ngOnInit() {}

  /**
   * @param formValue the form being submitted, containing the weather data
   * returns an observable returned from the post function found in
   * api
   */
  createWeatherData(formValue: any) {
    validateForm(this.createForm);

    if (this.createForm.valid) {
      const date = this.createForm.value['date'];
      const time = this.createForm.value['time'];

      this.createForm.value['date'] = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
      this.createForm.value['time'] = time.getHours() + ':' + time.getMinutes() + ':' + time.getSeconds();

      this.api.post('weatherData', this.createForm.value).subscribe(s => {});
      this.notification.createNotification('success', 'Weather data added successfully');
      this.createForm.reset();
    } else {
      this.notification.createNotification('error', 'Please enter new weather data');
    }
  }
}
