import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateWeatherDataComponent } from './create-weather-data.component';
import { ReactiveFormsModule, FormControl } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


describe('CreateWeatherDataComponent', () => {
  let component: CreateWeatherDataComponent;
  let fixture: ComponentFixture<CreateWeatherDataComponent>;
  let goodForm: any;
  let badForm: any;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CreateWeatherDataComponent
      ],
      imports: [
        ReactiveFormsModule,
        NgZorroAntdModule,
        HttpClientModule,
        BrowserAnimationsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateWeatherDataComponent);
    component = fixture.componentInstance;
    goodForm = {
      'date': new Date(2018, 11, 24, 10, 33, 30, 0),
      'speed': 15,
      'pressure': 30,
      'humidity': 45,
      'temperature': 300,
      'lon': 45.0,
      'lat': 50.0,
      'time': new Date(2018, 11, 24, 10, 33, 30, 0)
    };
    badForm = {
      'date': '2014-05-18',
      'speed': -1,
      'pressure': -1,
      'humidity': -1,
      'temperature': -1,
      'lon': -200,
      'lat': -200,
      'time': '19:05:40'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should create weather data if form is valid', () => {
    component.createForm.setValue(goodForm);
    expect(component.createForm.valid).toBeTruthy();
    component.createWeatherData(new FormControl());
    expect(component.createForm.pristine).toBeTruthy();
    expect(component.createForm.untouched).toBeTruthy();
  });

  it('Should not create weather data if form is invalid', () => {
    component.createForm.setValue(badForm);
    expect(component.createForm.valid).toBeFalsy();
    component.createWeatherData(new FormControl());
    expect(component.createForm.valid).toBeFalsy();
  });

});
