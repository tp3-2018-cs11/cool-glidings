import { ChartsModule } from 'ng2-charts';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, en_US} from 'ng-zorro-antd';
import { AppComponent } from './app.component';

/** config angular i18n **/
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { AppRoutingModule } from './app-routing.module';
import { LandingComponent } from './landing/landing.component';
import { LocationsPageComponent } from './locations-page/locations-page.component';
import { CreateWeatherDataComponent } from './create-weather-data/create-weather-data.component';
import { GeneratorsPageComponent } from './generators-page/generators-page.component';
import { EnergyRetrievalComponent } from './energy-retrieval/energy-retrieval.component';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    LocationsPageComponent,
    CreateWeatherDataComponent,
    GeneratorsPageComponent,
    EnergyRetrievalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    /** import ng-zorro-antd root module，you should import NgZorroAntdModule instead in sub module **/
    NgZorroAntdModule,
    AppRoutingModule,
    ReactiveFormsModule,
    ChartsModule
  ],
  bootstrap: [ AppComponent ],
  /** config ng-zorro-antd i18n **/
  providers   : [ { provide: NZ_I18N, useValue: en_US }, NgZorroAntdModule ]
})
export class AppModule { }
