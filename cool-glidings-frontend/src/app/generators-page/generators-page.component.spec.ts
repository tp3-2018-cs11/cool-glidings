import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneratorsPageComponent } from './generators-page.component';
import { HttpClientModule } from '@angular/common/http';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { ReactiveFormsModule, FormGroup, FormControl } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('GeneratorsPageComponent', () => {
  let component: GeneratorsPageComponent;
  let fixture: ComponentFixture<GeneratorsPageComponent>;
  let htmlElement: HTMLElement;
  let goodForm: any;
  let badForm: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneratorsPageComponent ],
      imports: [
        ReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        BrowserAnimationsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneratorsPageComponent);
    component = fixture.componentInstance;
    htmlElement = fixture.debugElement.nativeElement;
    component.ngOnInit();
    goodForm = {
      'label': 'PSG61',
      'powerCoefficient': '0.6',
      'radius': '10',
      'height': '10',
    };
    badForm = {
      'label': ' ',
      'powerCoefficient': '1.1',
      'radius': '0',
      'height': '-10',
    };
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have an h2 tag "Manage your generators"', async(() => {
    expect(htmlElement.querySelector('h2').textContent).toEqual('Manage your Generators');
  }));


  it('should have createGnerator form group', () => {
    expect(component.createGeneratorForm instanceof FormGroup).toBe(true);
  });

  it('create Generator should contain all fields', () => {
    expect(component.createGeneratorForm.controls['label']).toBeDefined();
    expect(component.createGeneratorForm.controls['powerCoefficient']).toBeDefined();
    expect(component.createGeneratorForm.controls['radius']).toBeDefined();
    expect(component.createGeneratorForm.controls['height']).toBeDefined();
    expect(component.createGeneratorForm.controls['undefined']).toBeUndefined();
  });

  it('should have editGenerator form group', () => {
    expect(component.editGeneratorForm instanceof FormGroup).toBe(true);
  });

  it('edit Generator should contain all fields', () => {
    expect(component.editGeneratorForm.controls['label']).toBeDefined();
    expect(component.editGeneratorForm.controls['powerCoefficient']).toBeDefined();
    expect(component.editGeneratorForm.controls['radius']).toBeDefined();
    expect(component.editGeneratorForm.controls['height']).toBeDefined();
    expect(component.editGeneratorForm.controls['undefined']).toBeUndefined();
  });

  it('Should delete a generator correctly', () => {
    component.data = [...component.data, goodForm];
    component.editGeneratorForm.setValue(goodForm);
    component.createGeneratorForm.setValue(goodForm);
    component.delete(goodForm.label);

    expect(component.data.filter(obj => obj.label === goodForm.label).length === 0).toBeTruthy();
    expect(component.editGeneratorForm.valid).toBeFalsy();
    expect(component.createGeneratorForm.valid).toBeTruthy();
  });

  it('Should create a generator correctly', () => {
    component.createGeneratorForm.setValue(goodForm);
    component.editGeneratorForm.setValue(goodForm);

    expect(component.createGeneratorForm.valid).toBeTruthy();

    component.createGenerator();

    expect(component.data.filter(obj => obj.label === goodForm.label).length === 1).toBeTruthy();
    expect(component.createGeneratorForm.pristine).toBeTruthy();
    expect(component.createGeneratorForm.untouched).toBeTruthy();
    expect(component.editGeneratorForm.valid).toBeTruthy();
  });

  it('Should not create a generator if createGeneratorForm is invalid', () => {
    component.createGeneratorForm.setValue(badForm);
    component.createGenerator();

    expect(component.data.filter(obj => obj.label === goodForm.label).length === 0).toBeTruthy();
    expect(component.createGeneratorForm.valid).toBeFalsy();
  });

  it('Should edit a generator correctly', () => {
    component.data = [...component.data, goodForm];
    const exampleForm = {
      'label': goodForm.label,
      'powerCoefficient': 0.6,
      'radius': 10,
      'height': 10,
    };

    component.editGeneratorForm.setValue(exampleForm);

    expect(component.editGeneratorForm.valid).toBeTruthy();

    component.editGenerator();
    expect(component.data.find(obj => obj.label === goodForm.label).powerCoefficient).toBe(0.6);
    expect(component.data.find(obj => obj.label === goodForm.label).radius).toBe(10);
    expect(component.data.find(obj => obj.label === goodForm.label).height).toBe(10);
    expect(component.editGeneratorForm.pristine).toBeTruthy();
    expect(component.editGeneratorForm.untouched).toBeTruthy();
  });

  it('Should not edit a generator if editGeneratorForm is invalid', () => {
    component.editGeneratorForm.setValue(badForm);
    component.editGenerator();
    expect(component.editGeneratorForm.valid).toBeFalsy();
  });

  it('Should populate the edit form correctly if the generator exists', () => {
    component.data = [...component.data, goodForm];
    component.editGeneratorForm.get('label').setValue(goodForm.label);

    component.selectChange(goodForm.label);

    expect(component.editGeneratorForm.get('powerCoefficient').value).toBe(goodForm.powerCoefficient);
    expect(component.editGeneratorForm.get('radius').value).toBe(goodForm.radius);
    expect(component.editGeneratorForm.get('height').value).toBe(goodForm.height);
    expect(component.editGeneratorForm.valid).toBeTruthy();
  });

  it('Test validGeneratorLabel: Should return true(error) if the generator already exists', () => {
    component.data = [...component.data, goodForm];
    expect(component.validGeneratorLabel(new FormControl(goodForm.label)).error).toBeTruthy();
  });

  it('Test validGeneratorLabel: Should return undefined(success) if the generator does not exists', () => {
    expect(component.validGeneratorLabel(new FormControl(goodForm.label))).toBe(undefined);
  });

  it('Test validGeneratorNameExists: Should return undefined(success) if the generator already exists', () => {
    component.data = [...component.data, goodForm];
    expect(component.validGeneratorNameExists(new FormControl(goodForm.label))).toBe(undefined);
  });

  it('Test validGeneratorNameExists: Should return true(error) if the generator does not exists', () => {
    expect(component.validGeneratorNameExists(new FormControl(goodForm.label)).error).toBeTruthy();
  });

  it('Should sort data ascending', () => {
    const exampleForm = {
      'label': 'AAA',
      'powerCoefficient': 0.6,
      'radius': 10,
      'height': 10,
    };

    component.data = [...component.data, goodForm];
    component.data = [...component.data, exampleForm];
    component.columnSort({'value': 'ascend', 'key': 'label'});

    expect(component.data.findIndex(obj => obj.label === exampleForm.label)).toBe(0);
    expect(component.data.findIndex(obj => obj.label === goodForm.label)).toBe(1);
  });

  it('Should sort data descending', () => {
    const exampleForm = {
      'label': 'AAA',
      'powerCoefficient': 0.6,
      'radius': 10,
      'height': 10,
    };

    component.data = [...component.data, goodForm];
    component.data = [...component.data, exampleForm];
    component.columnSort({'value': 'descend', 'key': 'label'});

    expect(component.data.findIndex(obj => obj.label === exampleForm.label)).toBe(1);
    expect(component.data.findIndex(obj => obj.label === goodForm.label)).toBe(0);
  });

});
