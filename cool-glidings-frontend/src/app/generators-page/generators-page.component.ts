import { Component, OnInit } from '@angular/core';
import { ApiCallsService } from './../Services/api/api-calls.service';
import { NotificationService } from 'src/app/Services/notification/notification.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { validPowerCoefficient, validPositiveValue, validateWhitespace, validateForm } from '../validators/validators';
@Component({
  selector: 'app-generators-page',
  templateUrl: './generators-page.component.html',
  styleUrls: ['./generators-page.component.less']
})
export class GeneratorsPageComponent implements OnInit {

  data: any[] = new Array();
  createGeneratorForm: FormGroup;
  editGeneratorForm: FormGroup;

  constructor(
    private api: ApiCallsService,
    private fb: FormBuilder,
    private notification: NotificationService) { }

  ngOnInit() {
    this.getGenerators();
    // Initialise the createGeneratorForm and set its validators
    this.createGeneratorForm = this.fb.group({
      label : [ null, [ Validators.required, this.validGeneratorLabel , validateWhitespace] ],
      powerCoefficient : [ null, [ Validators.required, validPowerCoefficient] ],
      radius : [ null, [ Validators.required, validPositiveValue] ],
      height : [ null, [ Validators.required, validPositiveValue] ]
    });

    // Initialise the editGeneratorform and set its validators
    this.editGeneratorForm = this.fb.group({
      label : [ null, [ Validators.required, validateWhitespace, this.validGeneratorNameExists] ],
      powerCoefficient : [ null, [ Validators.required, validPowerCoefficient] ],
      radius : [ null, [ Validators.required, validPositiveValue] ],
      height : [ null, [ Validators.required, validPositiveValue] ]
    });
  }

  /**
   * Returns an array of generators called from the server
   */
  getGenerators() {
    this.api.get('generators').subscribe(data => this.data = data);
  }


  /**
   * Deletes an element from both the data set and from the server
   */
  delete(key: string) {
    this.api.delete('generators', key).subscribe();
    this.data = this.data.filter(object => object.label !== key);
    this.notification.createNotification('success', 'Generator Deleted successfully');
    if (this.createGeneratorForm.get('label').value !== null) {
      validateForm(this.createGeneratorForm);
    }
    if (this.editGeneratorForm.get('label').value !== null) {
      validateForm(this.editGeneratorForm);
    }
  }

  /**
  * ======= CREATE GENERATOR FORM ===================================
  * Function executed when the createForm is submitted
  */
  createGenerator() {
    // Check that all fields are valid
    validateForm(this.createGeneratorForm);

    // If the form is valid, create a new generator and update the table
    if (this.createGeneratorForm.valid) {
      this.api.post('generators', this.createGeneratorForm.value).subscribe();
      this.data = [ ...this.data, this.createGeneratorForm.value];
      this.createGeneratorForm.reset();
      this.notification.createNotification('success', 'Generator Created successfully');
      if (this.editGeneratorForm.get('label').value !== null) {
        validateForm(this.editGeneratorForm);
      }
    }
  }
  // ====================================================================
  /**
   * ====================== EDIT GENERATOR FROM ==========================
   */
  // Function executed when the editGeneratorForm is submitted
  editGenerator() {
    // Check that all inputs are valid
    validateForm(this.editGeneratorForm);

    // If the generator is valid change it in both table and server
    if (this.editGeneratorForm.valid) {
      this.api.put('generators', this.editGeneratorForm.value.label, this.editGeneratorForm.value).subscribe();
      const index = this.data.findIndex(x => x.label === this.editGeneratorForm.value.label);
      this.data[index].powerCoefficient = this.editGeneratorForm.value.powerCoefficient;
      this.data[index].radius = this.editGeneratorForm.value.radius;
      this.data[index].height = this.editGeneratorForm.value.height;
      this.editGeneratorForm.reset();
      this.notification.createNotification('success', 'Generator Modified Successfully');
    }
  }
  // Function used to change populate the edit form once a generator
  // has been selected
  selectChange(key: string) {
    if (this.editGeneratorForm.get('label').value !== null) {
      this.editGeneratorForm.get('powerCoefficient').setValue(this.data.find(gen => gen.label === key).powerCoefficient);
      this.editGeneratorForm.get('radius').setValue(this.data.find(gen => gen.label === key).radius);
      this.editGeneratorForm.get('height').setValue(this.data.find(gen => gen.label === key).height);
      validateForm(this.editGeneratorForm);
    }
  }

  // ====================================================================
  // CUSTOM VALIDATORS FOR THE COMPONENT
  // Check if generator label does not exist. It returns true(error) if the label
  // exists in our dataset
  validGeneratorLabel = (control: FormControl): { [ s: string ]: boolean } => {
    if (this.data.findIndex(x => x.label === control.value) !== -1) {
      return {error: true};
    }
  }

  // Check if generator label exist. It returns true(error) if the label does
  // not exists in our dataset
  validGeneratorNameExists = (control: FormControl): { [ s: string ]: boolean } => {
    if (this.data.findIndex(x => x.label === control.value) === -1) {
      return {error: true};
    }
  }

  // =========================================================================

  /**
   * General function that can be used in order to sort table columns
   * It need an external boolean component's field to toggle the sorting
   * order.
   * It also takes a key, which is the attribute that we are using in order
   * to sort our data set. The key is provided by the $event parameter in
   * the template
   */
  columnSort(event: any) {
    this.data = [...this.data.sort((value1, value2) => {
        if (event.value === 'ascend') {
          return value1[event.key] < value2[event.key] ? -1 : 1;
        } else if (event.value === 'descend') {
          return value1[event.key] > value2[event.key] ? -1 : 1;
        }
     })];
  }

}
