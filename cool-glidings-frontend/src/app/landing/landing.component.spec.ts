import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingComponent } from './landing.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('LandingComponent', () => {
  let component: LandingComponent;
  let fixture: ComponentFixture<LandingComponent>;
  let htmlElement: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingComponent ],
      imports: [ BrowserAnimationsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingComponent);
    component = fixture.componentInstance;
    htmlElement = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have an h1 tag "Welcome to the Cool Glidings Web App!"', async(() => {
    expect(htmlElement.querySelector('h1').textContent).toEqual('Welcome to the Cool Glidings Web App!');
  }));

  it('should have an h2 tag "About"', async(() => {
    expect(htmlElement.querySelector('h2').textContent).toEqual('About');
  }));

  it('should contain "This is a web app project by University of Glasgow students"', async(() => {
    expect(htmlElement.textContent).toContain('This is a web app project by University of Glasgow students');
  }));

});
