import { EnergyRetrievalComponent } from './energy-retrieval/energy-retrieval.component';
import { LocationsPageComponent} from './locations-page/locations-page.component';
import { LandingComponent } from './landing/landing.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateWeatherDataComponent } from './create-weather-data/create-weather-data.component';
import { GeneratorsPageComponent } from './generators-page/generators-page.component';
const routes: Routes = [
  { path: '', component: LandingComponent, pathMatch: 'full' },
  { path: 'locations', component: LocationsPageComponent, pathMatch: 'full' },
  { path: 'generators', component: GeneratorsPageComponent, pathMatch: 'full' },
  { path: 'addWeatherData', component: CreateWeatherDataComponent, pathMatch: 'full' },
  { path: 'energyQuery', component: EnergyRetrievalComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
