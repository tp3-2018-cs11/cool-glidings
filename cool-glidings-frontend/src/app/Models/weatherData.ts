
export class WeatherData {
    date: string;
    speed: number;
    pressure: number;
    humidity:  number;
    temperature: number;
    lon: number;
    lat: number;
    time: string;

    constructor(date: string, speed: number, pressure: number, humidity: number,
                temperature: number, lon: number, lat: number, time: string) {
        this.date = date;
        this.speed = speed;
        this.pressure = pressure;
        this.humidity = humidity;
        this.temperature = temperature;
        this.lon = lon;
        this.lat = lat;
        this.time = time;
    }
}
