
export class Generator {
    powerCoefficient: number;
    radius: number;
    height: number;
    label: string;

    constructor(powerCoefficient: number, radius: number, height: number, label: string) {
        this.powerCoefficient = powerCoefficient;
        this.radius = radius;
        this.height = height;
        this.label = label;
    }
}
