import { Chart } from 'chart.js';
import { Validators } from '@angular/forms';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiCallsService } from 'src/app/Services/api/api-calls.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-energy-retrieval',
  templateUrl: './energy-retrieval.component.html',
  styleUrls: ['./energy-retrieval.component.less'],
})
export class EnergyRetrievalComponent implements OnInit {
  retrieveForm: FormGroup;
  locations: any;
  generators: any;
  energy: Array<any>;
  time: Date | null = null;
  time2: Date | null = null;
  url  = 'http://127.0.0.1:8000/cool_glidings/energyQuery/';
  chart: Chart;
  private months = {
    Jan: '01',
    Feb: '02',
    Mar: '03',
    Apr: '04',
    May: '05',
    Jun: '06',
    Jul: '07',
    Aug: '08',
    Sep: '09',
    Oct: '10',
    Nov: '11',
    Dec: '12'
  };

  constructor(private fb: FormBuilder, private api: ApiCallsService) {
    this.retrieveForm = this.fb.group({
      location: ['', Validators.required],
      generator: ['', Validators.required],
      startDate: [''],
      endDate: [''],
      startTime: [this.time],
      endTime: [this.time2]
    });
  }

  ngOnInit() {
    this.getLocations();
    this.getGenerators();
  }

  /**
   * @param formValue - the form being submitted
   * Returns true if the start date has been filled in
   */
  hasEntry(formValue: any, field: string): boolean {
    const value = formValue[field];
    if (value === '' || value === null || value === undefined ) {
      if (field === 'startDate') {
        this.retrieveForm.value['startTime'] = '';
      } else {
        this.retrieveForm.value['endTime'] = '';
      }
      return false;
    }
    return true;
  }

  /**
   * Stores the locations from the database in the locations variable
   */
  private getLocations() {
    this.api.get('locations').subscribe(s => {
      this.locations = s;
    });
  }

  /**
   * Stores the generators from the database in the generators variable
   */
  private getGenerators() {
    this.api.get('generators').subscribe(s => {
      this.generators = s;
    });
  }

  /**
   * @param formValue - the form being submitted
   * Stores the energy sent from the database inside
   * the energy variable
   */
  getEnergy(formValue: any) {
    this.cleanDate('startDate');
    this.cleanDate('endDate');
    this.cleanTime('startTime');
    this.cleanTime('endTime');
    this.api
      .getEnergy(
        formValue['location'],
        formValue['generator'],
        formValue['startDate'],
        formValue['endDate'],
        formValue['startTime'],
        formValue['endTime']
      )
      .subscribe(s => {
        this.energy = s;
        this.getData();
      });
  }

  /**
   * @param formValue the form being submitted
   * Returns true if the form can be submitted
   */
  validateForm(formValue: any) {
    return formValue['location'] !== '' && formValue['generator'] !== '';
  }

  /**
   * @param formValue - the form being submitted
   * @param field - the field being processed into the correct format
   * Helper function which cleans up the format of the date giving from the date picker
   */
  cleanDate(field: string) {
    let date = '';
    const value = this.retrieveForm.value[field];
    if (value !== '' && value !== undefined && value !== null) {
      date = `${value}`;
      const splitDate = date.split(' ');
      const tempDate = [splitDate[3], this.months[splitDate[1]], splitDate[2]];
      date = `${tempDate[0]}-${tempDate[1]}-${tempDate[2]}`;
    }
    this.retrieveForm.value[field] = date;
  }

  /**
   * @param formValue - the form being submitted
   * @param field - the field being processed into the correct format
   * Helper function which cleans up the format of the time giving from the time picker
   */
  cleanTime(field: string) {
    let time = '';
    const value = this.retrieveForm.value[field];
    if (value !== '' && value !== undefined && value !== null) {
      time = `${value.toString()}`;
      const splitTime = time.split(' ');
      time = splitTime[4];
      time = time[0] + time[1] + time[2] + time[3] + time[4];
    }
    this.retrieveForm.value[field] = time;
  }

  /**
   * Passes all the data values to the chart variables
   */
  getData() {
    if (this.energy && this.energy.length > 0) {
      const energyResults: Array<any> = [];
      const labels: Array<any> = [];
      for (let i = 0; i < this.energy.length; i++) {
        energyResults.push(this.energy[i]['energy']);
        // If the current date is not in the result Array append it to it.
        if (!labels.includes(this.energy[i]['date'])) {
          labels.push(this.energy[i]['date']);
        } else {
          labels.push(this.energy[i]['time']);
        }
      }
      this.drawGraph(energyResults, labels);
      // append the necesary parameters to the url
      this.url += `?loc=${this.retrieveForm.value['location']}`;
      this.url += `&gen=${this.retrieveForm.value['generator']}`;
      const params = ['startDate', 'endDate', 'startTime', 'endTime'];
      for (let i = 0; i < params.length; i++) {
        const value = this.retrieveForm.value[params[i]];
        if (value !== null && value !== undefined && value !== '') {
          this.url += `&${params[i]}=${value}`;
        }
      }
      this.url += '&format=csv';
    }
  }

  /**
   * Draws the graph from the results
   */
  drawGraph(energyData: any[], labels: any[]) {
    // Delete the existing chart if we wish to draw another one
    if (this.chart) {
      this.chart.destroy();
    }
    const canvas = <HTMLCanvasElement> document.getElementById('myCanvas');
    canvas.style.display = 'block';
    const ctx = canvas.getContext('2d');
    this.chart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labels,
          datasets: [{
            label: 'Power Generated from Wind Turbine',
            steppedLine: false,
            fill: true,
            backgroundColor: 'rgba(62,144,247,0.2)',
            borderColor: 'rgba(62,144,247,1)',
            pointBackgroundColor: '#fff',
            pointBorderColor: 'rgba(62,144,247,1)',
            pointHoverBackgroundColor: 'rgba(62,144,247,1)',
            pointHoverBorderColor: '#fff',
            data: energyData,
          }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          legend: {
              display: true,
          },
          scales: {
            yAxes: [{
              scaleLabel: {
                display: true,
                labelString: 'Power (W)',
              }
            }],
            xAxes: [{
              scaleLabel: {
                display: true,
                labelString: 'Date (dd-mm-yyyy)',
              }
            }],
          }
        }
    });
    this.chart.update();
  }

}
