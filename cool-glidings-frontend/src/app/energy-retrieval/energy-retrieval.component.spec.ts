import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnergyRetrievalComponent } from './energy-retrieval.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';



describe('EnergyRetrievalComponent', () => {
  let component: EnergyRetrievalComponent;
  let fixture: ComponentFixture<EnergyRetrievalComponent>;
  let goodForm: any;
  let badForm: any;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EnergyRetrievalComponent,
      ],
      imports: [
        ReactiveFormsModule,
        NgZorroAntdModule,
        HttpClientModule,
        BrowserAnimationsModule,
        ChartsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnergyRetrievalComponent);
    component = fixture.componentInstance;
    component.energy = ['test'];
    goodForm = {
      'location': 'Carcant Wind Farm',
      'generator': 'F47C489',
      'startDate': '2019-02-02',
      'endDate': '2019-03-02',
    };
    badForm = {
      'location': '',
      'generator': '',
      'startDate': '',
      'endDate': ''
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return true as the form is valid', () => {
    expect(component.validateForm(goodForm)).toBeTruthy();
  });

  it('should return false as the form is invalid', () => {
    expect(component.validateForm(badForm)).toBeFalsy();
  });

  it('should format the date correctly for the api call', () => {
    component.retrieveForm.value['startTime'] = 'Tue Feb 12 2019 21:14:22 GMT+0000 (Greenwich Mean Time)';
    component.cleanDate('startTime');
    expect(component.retrieveForm.value['startTime']).toEqual('2019-02-12');
  });

  it('shoud format the time correctly for the api call', () => {
    component.retrieveForm.value['startTime'] = 'Tue Feb 12 2019 21:14:22 GMT+0000 (Greenwich Mean Time)';
    component.cleanTime('startTime');
    expect(component.retrieveForm.value['startTime']).toEqual('21:14');
  });

  it('should return true as startDate and endDate are filled in', () => {
    expect(component.hasEntry(goodForm, 'startDate')).toBeTruthy();
    expect(component.hasEntry(goodForm, 'endDate')).toBeTruthy();
  });

  it('should return false as startDate and endDate are not filled in', () => {
    expect(component.hasEntry(badForm, 'startDate')).toBeFalsy();
    expect(component.hasEntry(badForm, 'endDate')).toBeFalsy();
  });

  it('should make the canvas element in the html visible after a valid form has been submitted', () => {
    expect(document.getElementById('myCanvas').style.display).toEqual('none');
    component.drawGraph([1, 2, 3, 4], ['a', 'b', 'c', 'd']);
    expect(document.getElementById('myCanvas').style.display).toEqual('block');
    expect(component.chart).toBeDefined();
    // This is called again, as once chart is defined, it is
    // destroyed on every call to the same function when
    // a new graph is being drawn
    component.drawGraph([1, 2, 3, 4], ['a', 'b', 'c', 'd']);
    expect(component.chart).toBeDefined();
  });

  it('should give the url in the correct format in order to make the csv api call', () => {
    component.retrieveForm.value['generator'] = 'generator';
    component.retrieveForm.value['location'] = 'location';
    component.retrieveForm.value['startDate'] = '2019-02-12';
    component.retrieveForm.value['startTime'] = '21:14';
    component.retrieveForm.value['endDate'] = '2019-03-12';
    component.retrieveForm.value['endTime'] = '21:14';
    component.getData();
    let url = 'http://127.0.0.1:8000/cool_glidings/energyQuery/';
    url += '?loc=location&gen=generator&startDate=2019-02-12&endDate=2019-03-12&startTime=21:14&endTime=21:14&format=csv';
    expect(component.url).toEqual(url);
  });

});
