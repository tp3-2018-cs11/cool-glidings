import { TestBed } from '@angular/core/testing';

import { NotificationService } from './notification.service';
import { NgZorroAntdModule } from 'ng-zorro-antd';

describe('NotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [NgZorroAntdModule]
  }));

  it('should be created', () => {
    const service: NotificationService = TestBed.get(NotificationService);
    expect(service).toBeTruthy();
  });
});
