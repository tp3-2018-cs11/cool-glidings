import { Injectable } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private notification: NzNotificationService) { }

  /**
   * Creates a notification when a certain event occurs.
   * The function accepts:
   * - a type string which represents the type of the message
   * - a message title
   * - an optional message content
   * - an optional message duration
  */
  public createNotification(type: string, title: string, content = '', time = 3000): void {
    this.notification.create(type, title, content, { nzDuration: time });
  }

}

