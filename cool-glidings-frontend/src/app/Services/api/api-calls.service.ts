import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { NotificationService } from '../notification/notification.service';
import { catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ApiCallsService {
  private restUrl = 'http://127.0.0.1:8000/cool_glidings/';

  constructor(private http: HttpClient, private notify: NotificationService) {}

  /**
   * @param model - model being returned
   * Makes a get request to the api and returns an array of
   * the chosen model from the restAPI
   */
  get(model: string): Observable<any[]> {
    const url = `${this.restUrl}${model}/`;
    const request = this.http.get<any>(url);
    return request.pipe(catchError(this.handleError('get', [])));
  }

  /**
   * @param model - model being inserted
   * @param obj - object (can either be weather data, location or generators)
   * Makes a post request to the api and returns an
   * observable of the chosen model
   */
  post(model: string, obj: any): Observable<any> {
    const url = `${this.restUrl}${model}/`;
    const request = this.http.post<any>(url, obj);
    return request.pipe(catchError(this.handleError<any>('post')));
  }

  /**
   * @param model - model being inserted
   * @param id - id of object being deleted
   * @param obj - object being updated
   * Makes a put request to the api and returns an
   * observable of the chosen model
   */
  put(model: string, id: any, obj: any): Observable<any> {
    const url = `${this.restUrl}${model}/${id}/`;
    const request = this.http.put<any>(url, obj);
    return request.pipe(catchError(this.handleError<any>('put')));
  }

  /**
   * @param model - model being deleted
   * @param id - id of object being deleted
   * Makes a delete request to the api and returns
   * an observable to the chosen model
   */
  delete(model: string, id: any): Observable<any> {
    const url = `${this.restUrl}${model}/${id}/`;
    const request = this.http.delete<any>(url);
    return request.pipe(catchError(this.handleError<any>('delete')));
  }

  /**
   * @param location - the location being queried for the energy calculation
   * @param generator - the generator being used in the calculation
   * Makes a get request to the api to get the energy output form
   * the input parameters and returns an observable to this
   * returned object from the server
   */
  getEnergy(
    location: string,
    generator: string,
    startDate: string,
    endDate: string,
    startTime: string,
    endTime: string,
  ): Observable<any> {
    let url = `${
      this.restUrl
    }energyQuery/?loc=${location}&gen=${generator}`;
    const params = ['startDate', 'endDate', 'startTime', 'endTime'];
    for (let i = 0; i < params.length; i++) {
      const value = arguments[i + 2];
      if (value !== null && value !== undefined && value !== '') {
        url += `&${params[i]}=${value}`;
      }
    }
    const request = this.http.get<any>(url);
    return request.pipe(catchError(this.handleError('get', [])));
  }

  /**
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   * Handle Http operation that failed.
   * Let the app continue.
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      let errorInfo = '';
      let errorType = '';
      console.log(error); // log to console instead
      if (error.status === 400) {
        errorType = 'error';
        errorInfo = 'Your request could not be met due to some invalid input we did not pick up';
      } else if (error.status === 404) {
        errorType = 'error';
        errorInfo = 'Resource could not be found';
      } else if (error.status === 422) {
        errorType = 'warning';
        errorInfo = 'We did not have enough data to process your request';
      } else {
        errorType = 'error';
        errorInfo = 'Something went wrong on our end...sorry';
      }

      this.notify.createNotification(errorType, 'Could not process Request', errorInfo);

      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
