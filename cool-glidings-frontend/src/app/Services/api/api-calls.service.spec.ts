import { Generator } from './../../Models/generator';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ApiCallsService } from './api-calls.service';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('ApiCallsService', () => {
  let api: ApiCallsService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        NgZorroAntdModule,
        BrowserAnimationsModule
      ],
      providers: [
        ApiCallsService
      ]
    });
    api = TestBed.get(ApiCallsService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should make a get request to the rest api', (done) => {
    api.get('generators')
      .subscribe(s => {
        expect(s).toEqual(
          [
            {
              'powerCoefficient': 0.4,
              'radius': 50,
              'height': 150,
              'label': 'FC4789'
            },
            {
              'powerCoefficient': 0.7,
              'radius': 40,
              'height': 140,
              'label': 'FC479'
            }
          ]
        );
      });
    const generatorRequest = httpMock.expectOne('http://127.0.0.1:8000/cool_glidings/generators/');
    generatorRequest.flush(
      [
        {
          'powerCoefficient': 0.4,
          'radius': 50,
          'height': 150,
          'label': 'FC4789',
        },
        {
          'powerCoefficient': 0.7,
          'radius': 40,
          'height': 140,
          'label': 'FC479'
        }
      ]
    );
    expect(generatorRequest.request.method).toBe('GET');
    done();
    httpMock.verify();
  });

  it('should make a post request to the rest api', (done) => {
    const generator = new Generator(0.5, 40, 120, 'F4789');
    api.post('generators', generator)
      .subscribe(s => {
        expect(s).toEqual({
          'powerCoefficient': 0.5,
          'radius': 40,
          'height': 120,
          'label': 'F4789'
        });
      });
    const generatorRequest = httpMock.expectOne('http://127.0.0.1:8000/cool_glidings/generators/');
    generatorRequest.flush({
      'powerCoefficient': 0.5,
      'radius': 40,
      'height': 120,
      'label': 'F4789'
    });
    expect(generatorRequest.request.method).toBe('POST');
    done();
    httpMock.verify();
  });

  it('should make a delete request to the rest api', (done) => {
    api.delete('generators', 'F47C489')
      .subscribe(s => {
        expect(s).toEqual('F47C489');
      });
    const generatorRequest = httpMock.expectOne('http://127.0.0.1:8000/cool_glidings/generators/F47C489/');
    generatorRequest.flush('F47C489');
    expect(generatorRequest.request.method).toBe('DELETE');
    done();
    httpMock.verify();
  });

  it('should make a post request to the API', (done) => {
    const generator = new Generator(0.5, 50, 150, 'F47C489');
    api.put('generators', 'F47C489', generator)
      .subscribe(s => {
        expect(s).toEqual({
          'powerCoefficient': 0.4,
          'radius': 50,
          'height': 150,
          'label': 'FC4789'
        });
      });
    const generatorRequest = httpMock.expectOne('http://127.0.0.1:8000/cool_glidings/generators/F47C489/');
    generatorRequest.flush({
      'powerCoefficient': 0.4,
      'radius': 50,
      'height': 150,
      'label': 'FC4789',
    });
    expect(generatorRequest.request.method).toBe('PUT');
    done();
    httpMock.verify();
  });

  it('should make a get request to the API for energy', (done) => {
    api.getEnergy('location', 'generator', '2019-02-12', '2019-03-12', '21:14', '21:14')
      .subscribe();
    let url = 'http://127.0.0.1:8000/cool_glidings/energyQuery/';
    url += '?loc=location&gen=generator&startDate=2019-02-12&endDate=2019-03-12&startTime=21:14&endTime=21:14';
    const energyRequest = httpMock.expectOne(url);
    expect(energyRequest.request.method).toBe('GET');
    done();
    httpMock.verify();
  });

});
